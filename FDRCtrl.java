package com.svm.lrp.ops.fdr.ctrl;

import com.svm.lrp.ops.fdr.model.FDRModel;
import com.svm.lrp.ops.fdr.vo.FDRforcedDischargeVO;
import com.svm.lrp.ops.fdr.vo.FDRmatchTableVo;
import com.svm.lrp.ops.fdrejb.fdrbeanLocal;
import com.svm.lrp.nfr.jsf.utils.ILogger;
import com.svm.lrp.nfr.jsf.utils.NFRUtils;
import com.svm.lrp.nfr.jsf.utils.SMDialogUtil;
import com.svm.lrp.nfr.jsf.utils.ToolBarUtils;
import com.svm.lrp.nfr.jsf.utils.ToolbarState;
import com.svm.lrp.nfr.jsf.utils.UserData;
import com.svm.lrp.nfr.main.bean.ToolbarCBean;
import com.svm.lrp.nfr.search.utils.SearchCondition;
import com.svm.lrp.nfr.search.utils.SearchConditionBuilder;
import com.svm.lrp.nfr.search.utils.SearchUtils;
//import com.svm.nfr.component.datatable.DefaultColumnModel;
import com.svm.nfr.filter.GFilterUtil;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;

public class FDRCtrl {

    FDRModel model;
    FDRCtrl ctrl;
    ToolbarCBean bean;
    fdrbeanLocal remote;
    ILogger log;
    List condition;
    SearchConditionBuilder dynamicCondition;
    SMDialogUtil dutil = new SMDialogUtil();
    UserData mData;
    GFilterUtil futil;
    private int popconflt = 30;
    private int popconfgt = 10;
    private String VLC = "";
    private String CON = "";
    private String FDRL = "";
    private String List = "";
    String secondparam = "";
    FDRforcedDischargeVO selops = new FDRforcedDischargeVO();
    boolean finalmsg = false;
    private String finalmsgs = "";
    private static final String FDR = "FDR";
    private static final String SELECTALL = "Select All";
    private static final String LOCAL = "Local";
    private static final String TRANSHIPMENT = "Transhipment";
    private static final String TABLE1 = "FDR-FDR_FDGrid";
    private static final String TABLE2 = "FDR-FDR_MatchingGrid";
    private static final String FRAGB = "FDR-FDR_FRAGB";
    private static final String FRAGC = "FDR-FDR_FRAGC";
    private static final String FRAGB1 = "FDR-FDR_FRAGL";
    private static final String FRAG = "FDR-FDR_Frag";
    private static final String FRAG1 = "FDR-FDR_FRAG1";

    private static final String FDRCBEAN = "fdrCBean";
    private static final String CNTNO = "Container No";
     private static final String VINNO = "VIN/Chassis No";
    private static final String CNTNUMBER = "Container Number";
    private static final String BKGNO = "Book No / WO No";
    private static final String FDR06020012 = "06020012";
    private static final String FDR06020013 = "06020013";
    private static final String FDRNEWSVC = "FDR-FDR_txtNewSvc";
    private static final String FDR06020017 = "06020017";
    private static final String FDR06020018 = "06020018";
    private static final String FDR06020019 = "06020019";
    private static final String FDR06020020 = "06020020";
    private static final String FDRNEWTML = "FDR-FDR_txtNewOpspodTer";
    private static final String BKGNUMBER = "Book Number";
    private static final String FDRNEWVSL = "FDR-FDR_txtNewVslCode";
    private static final String FDRNEWVSLNAME = "FDR-FDR_txtNewVslName";
    private static final String MATCH = "Match";
    private static final String SOUT = "Exception in operation modules";
    private String PAGINATOR = "20,30,40";

    private boolean contract = false;
    private boolean remMsg = false;
    private String OPTIONBUTTON = "";
    private List CONTCON;
  String hcsplugin = "";
    public FDRCtrl(FDRModel md, ToolbarCBean bn, fdrbeanLocal rm, ILogger log1, GFilterUtil gf) {
        model = md;
        bean = bn;
        log = log1;
        futil = gf;
        remote = rm;
    }

    public FDRModel getMdl() {
        return model;
    }

    public void setMdl(FDRModel model) {
        this.model = model;
    }

    public FDRCtrl getCtrl() {
        return ctrl;
    }

    public void setCtrl(FDRCtrl ctrl) {
        this.ctrl = ctrl;
    }

    private void cacheconfig() {
        try {
            log.info("inside cacheconfig >> ");
//            CheckCode chckcode = new CheckCode();
//            ConditionModel conditionModel = new ConditionModel();
//            CacheCondition cachecondition = new CacheCondition("name", ConditionModel.where.EQUALS, "Paginator");
//            conditionModel.getCondition().add(cachecondition);
//            AutoModel automodel = chckcode.getObjectAsModel("optconfig", conditionModel);
//            if (automodel != null) {
//                PAGINATOR = automodel.getItemName();
//            }
            model.getUiDataModel().setRowsperpage(PAGINATOR);
            String rows[] = PAGINATOR.split(",");
            model.getUiDataModel().setRows(rows[0]);
            log.info("Paginator Value >> " + PAGINATOR);

//            chckcode = new CheckCode();
//            conditionModel = new ConditionModel();
//            cachecondition = new CacheCondition("name", ConditionModel.where.EQUALS, "PLCD");
//            conditionModel.getCondition().add(cachecondition);
//            automodel = chckcode.getObjectAsModel("optconfig", conditionModel);
//            if (automodel != null) {
//                popconflt = Integer.parseInt(automodel.getItemName());
//                log.info("PLCD Value >> " + popconflt);
//            }
//
//            chckcode = new CheckCode();
//            conditionModel = new ConditionModel();
//            cachecondition = new CacheCondition("name", ConditionModel.where.EQUALS, "PGCD");
//            conditionModel.getCondition().add(cachecondition);
//            automodel = chckcode.getObjectAsModel("optconfig", conditionModel);
//            if (automodel != null) {
//                popconfgt = Integer.parseInt(automodel.getItemName());
//                log.info("PGCD Value >> " + popconfgt);
//            }
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void createGUI() {
        try {
            mData = NFRUtils.getUserData();

            log.info("create gui called-------->>>");
            cacheconfig();
            getConfigurations();
             hcsplugin = remote.getConfiguration();
            log.info(hcsplugin + " hs plugin added***");
            
            if ("true".equalsIgnoreCase(hcsplugin)) {
                model.getStateModel().setRoroflag(true);
               
                            model.getStateModel().setContflag(false);
                             model.getUiDataModel().setButtonGroup2(BKGNO);
                              model.getStateModel().setGridhdr1(true);
            model.getUiDataModel().setBookcntrno("Book No / WO No");
            model.getUiDataModel().setLblStats("~ Matched Book No / WO No");
            model.getUiDataModel().setLblStats1("~ Unmatched Book No / WO No");

            } else {
                model.getStateModel().setRoroflag(false);
              
                            model.getStateModel().setContflag(true);
                            model.getUiDataModel().setButtonGroup2(BKGNO);
                              model.getStateModel().setGridhdr1(false);
            model.getUiDataModel().setBookcntrno("Book No / WO No");
            model.getUiDataModel().setLblStats("~ Matched Book No / WO No");
            model.getUiDataModel().setLblStats1("~ Unmatched Book No / WO No");

            }
            if (FDRL.equalsIgnoreCase("true")) {
                model.getUiDataModel().setSearchid("6020020");
            } else {
                model.getUiDataModel().setSearchid("6020014");
            }

            //model.getUiDataModel().setButtonGroup2(BKGNO);
            model.getUiDataModel().setBtnSelectValue(SELECTALL);
            model.getUiDataModel().setCmbPlanlist(new ArrayList());
            model.getStateModel().setAsLocal(false);
            model.getUiDataModel().getCmbPlanlist().add("");
            model.getUiDataModel().getCmbPlanlist().add(LOCAL);
            model.getUiDataModel().getCmbPlanlist().add(TRANSHIPMENT);
            //            log.info("model.getUiDataModel()...->0" + model.getUiDataModel().getCmbPlan());

            

            disableComponent();
            model.getUiDataModel().setGridStyle("sm-margin-right2 sm-table-wrap sm-border");
            model.getUiDataModel().setTable1size("ui-g-12 ui-lg-9 ui-md-12 sm-padding0 sm-margin-top0 sm-input-size84");
            model.getUiDataModel().setTable2size("ui-g-12 ui-lg-3 ui-md-12 sm-padding0 sm-margin-top0 sm-input-size16");
            model.getStateModel().setChkmatch(true);
            model.getStateModel().setChkmatchdis(true);
           // model.getStateModel().setChkmatchun(false);
            //model.getStateModel().setChkmatchundis(true);
            model.getUiDataModel().setfDGridList(new ArrayList());
            model.getUiDataModel().setMatchingGridList(new ArrayList());
//            radiouncontchange();
            
         
            if (model.getStateModel().isChkmatchun()) {
               
                model.getStateModel().setGridhdr3(false);
                model.getStateModel().setContflag(false);
                // model.getStateModel().setGridhdr4(true);
            } else {
                
                model.getStateModel().setGridhdr3(true);
                model.getStateModel().setContflag(true);
                // model.getStateModel().setGridhdr4(false);
            }
            RequestContext.getCurrentInstance().update(TABLE1);
            RequestContext.getCurrentInstance().update(FRAG1);
            
            
//            createDataTableColumnModel();
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

   /* private void createDataTableColumnModel() {
        try {
            model.getUiDataModel().setColumnModelList(new ArrayList());

            DefaultColumnModel df = new DefaultColumnModel();
            df.setHeaderName("Select");
            df.setField("select");
            df.setCheckboxSelection(true);
            df.setHeaderCheckboxSelection(true);
            model.getUiDataModel().getColumnModelList().add(df);

            df = new DefaultColumnModel();
            df.setHeaderName("Operation Status");
            df.setField("opsStatusL");
            model.getUiDataModel().getColumnModelList().add(df);

            df = new DefaultColumnModel();
            df.setHeaderName("bkgno");
            df.setField("bookNoL");
            model.getUiDataModel().getColumnModelList().add(df);

            df = new DefaultColumnModel();
            df.setHeaderName("Container No");
            df.setField("containerNoL");
            model.getUiDataModel().getColumnModelList().add(df);

            df = new DefaultColumnModel();
            df.setHeaderName("eqptype");
            df.setField("eqptype");
            model.getUiDataModel().getColumnModelList().add(df);

            df = new DefaultColumnModel();
            df.setHeaderName("Tran.Movement");
            df.setField("loadingConditionL");
            model.getUiDataModel().getColumnModelList().add(df);

            df = new DefaultColumnModel();
            df.setHeaderName("OPSPOL");
            df.setField("oPSPOLL");
            model.getUiDataModel().getColumnModelList().add(df);

            df = new DefaultColumnModel();
            df.setHeaderName("OPSPOL Terminal");
            df.setField("oPSPOLTerminalL");
            model.getUiDataModel().getColumnModelList().add(df);

            df = new DefaultColumnModel();
            df.setHeaderName("OPSPOD");
            df.setField("oPSPODL");
            model.getUiDataModel().getColumnModelList().add(df);

            df = new DefaultColumnModel();
            df.setHeaderName("OPSPOD Terminal");
            df.setField("oPSPODTerminalL");
            model.getUiDataModel().getColumnModelList().add(df);

            df = new DefaultColumnModel();
            df.setHeaderName("PLA");
            df.setField("pLAL");
            model.getUiDataModel().getColumnModelList().add(df);

            df = new DefaultColumnModel();
            df.setHeaderName("POL");
            df.setField("pOLL");
            model.getUiDataModel().getColumnModelList().add(df);

            df = new DefaultColumnModel();
            df.setHeaderName("POL Terminal");
            df.setField("pOLTerminalL");
            model.getUiDataModel().getColumnModelList().add(df);

            df = new DefaultColumnModel();
            df.setHeaderName("POD");
            df.setField("pODL");
            model.getUiDataModel().getColumnModelList().add(df);

            df = new DefaultColumnModel();
            df.setHeaderName("POD Terminal");
            df.setField("pODTerminalL");
            model.getUiDataModel().getColumnModelList().add(df);

            df = new DefaultColumnModel();
            df.setHeaderName("PLD");
            df.setField("pLDL");
            model.getUiDataModel().getColumnModelList().add(df);

            df = new DefaultColumnModel();
            df.setHeaderName("Service");
            df.setField("serviceL");
            model.getUiDataModel().getColumnModelList().add(df);

            df = new DefaultColumnModel();
            df.setHeaderName("Vessel");
            df.setField("vesselL");
            model.getUiDataModel().getColumnModelList().add(df);

            df = new DefaultColumnModel();
            df.setHeaderName("Voyage");
            df.setField("voyageL");
            model.getUiDataModel().getColumnModelList().add(df);

            df = new DefaultColumnModel();
            df.setHeaderName("Bound");
            df.setField("boundL");
            model.getUiDataModel().getColumnModelList().add(df);

            df = new DefaultColumnModel();
            df.setHeaderName("vendor");
            df.setField("vendor");
            model.getUiDataModel().getColumnModelList().add(df);

        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }
*/
    public void getConfigurations() {

        try {

            SearchUtils su = new SearchUtils();
            popconflt = Integer.parseInt(su.checkCode(FDR, "6020016"));

            popconfgt = Integer.parseInt(su.checkCode(FDR, "6020017"));

            VLC = su.checkCode(FDR, "6020018");
            CON = su.checkCode(FDR, "6020019");
            FDRL = su.checkCode(FDR, "6020021");

            log.info("VLC >> " + VLC);
            log.info("CON >> " + CON);
            log.info("FDRL >> " + FDRL);

        } catch (Exception e) {
            log.info(SOUT);

            log.fatal(e);
        }
    }

    public void disableComponent() {
        try {
            model.getStateModel().setBtnFilter(true);
            model.getStateModel().setjBtnSerDis(true);
            model.getStateModel().setjBtnVslDis(true);
            model.getStateModel().setjBtnVygDis(true);
            model.getStateModel().setBtnPortDis(true);
            model.getStateModel().setBtnterminalDis(true);
            model.getStateModel().setBtnShowDis(true);
            model.getStateModel().setCheck1Dis(true);
            model.getStateModel().setBtnErrorDis(true);

            model.getStateModel().setBtnConfDis(true);
            model.getStateModel().setCmbPlanDis(true);

            model.getStateModel().setButtonGroup2Dis(true);
            model.getStateModel().setSetSvcDetDis(true);
            model.getStateModel().setSvcRefreshDis(true);
            model.getStateModel().setTxtNewSvcDis(true);
            model.getStateModel().setBtnNewSvcDis(true);
            model.getStateModel().setNewSerRO(true);
            model.getStateModel().setTxtNewVslCodeDis(true);
            model.getStateModel().setBtnNewVslDis(true);
            model.getStateModel().setTxtNewVslNameDis(true);
            model.getStateModel().setTxtNewVoyDis(true);
            model.getStateModel().setBtnNewVoyBndDis(true);
            model.getStateModel().setTxtNewBndDis(true);
            model.getStateModel().setTxtNewOpspodDis(true);
            model.getStateModel().setBtnNewOpspodDis(true);
            model.getStateModel().setBtnNewOpspodTerDis(true);
            model.getStateModel().setTxtNewOpspodTerDis(true);

            model.getStateModel().setTxtServiceDis(true);
            model.getStateModel().setTxtVslCodeDis(true);
            model.getStateModel().setTxtVslNameDis(true);

            model.getStateModel().setTxtVoyageCodeDis(true);
            model.getStateModel().setTxtBoundDis(true);
            model.getStateModel().setTxtOpspodDis(true);
            model.getStateModel().setTxtterminalDis(true);
            model.getStateModel().setSetSvcDet(false);
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void treeTable1MouseClickedEvent(FDRforcedDischargeVO vo) {
        try {

            model.getUiDataModel().setSelectedGridValue(new ArrayList());

            for (int j = 0; j < model.getUiDataModel().getfDGridList().size(); j++) {
                FDRforcedDischargeVO vo1 = (FDRforcedDischargeVO) model.getUiDataModel().getfDGridList().get(j);
                if (vo1.getSelectL()) {
                    vo1 = (FDRforcedDischargeVO) model.getUiDataModel().getfDGridList().get(j);
                    model.getUiDataModel().getSelectedGridValue().add(vo1);
                } else {
                    model.getStateModel().setSelectenable(false);
                    model.getUiDataModel().getSelectedGridValue().remove(vo1);
                }
            }

            checkuncheck();

            RequestContext.getCurrentInstance().update(TABLE1);
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void checkuncheck() {
        boolean uncheckedsts = false;
        List gridList = new ArrayList();
        if (model.getUiDataModel().getFilterlist() != null && !model.getUiDataModel().getFilterlist().isEmpty()) {
            gridList = model.getUiDataModel().getFilterlist();

        } else if (model.getUiDataModel().getfDGridList() != null && !model.getUiDataModel().getfDGridList().isEmpty()) {
            gridList = model.getUiDataModel().getfDGridList();

        }
        if (!gridList.isEmpty()) {
            for (Object gridList1 : gridList) {
                FDRforcedDischargeVO vo = (FDRforcedDischargeVO) gridList1;
                if (!vo.getSelectL()) {
                    uncheckedsts = true;
                    break;
                }
            }
        } else {
            uncheckedsts = false;
        }
        if (!uncheckedsts) {
            model.getStateModel().setSelectenable(true);
        } else {
            model.getStateModel().setSelectenable(false);
        }
    }

    public void jBtnSerActionPerformed(ActionEvent event) {
        try {

            SearchUtils su = new SearchUtils();
            mData = NFRUtils.getUserData();
            su.showTwoColSearch();
             if (model.getStateModel().isChkmatchun()) {
            su.invokeTwoColSearch(FDRCBEAN, "060200011", event);
             }else
             {
               su.invokeTwoColSearch(FDRCBEAN, "06020001", event);   
             }
        

        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void jBtnVslActionPerformed(ActionEvent event) {
        try {
            if (model.getUiDataModel().getTxtService().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, FDR06020012, "", null);
            } else {

                SearchUtils su = new SearchUtils();
                mData = NFRUtils.getUserData();
                su.showTwoColSearch();
                su.setParameterValues(0, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtService());
                
               if (model.getStateModel().isChkmatchun()) {
            su.invokeTwoColSearch(FDRCBEAN, "060200022", event);
             }else
             {
               su.invokeTwoColSearch(FDRCBEAN, "06020002", event);   
             }
            }
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void jBtnVygActionPerformed(ActionEvent event) {
        try {
            if (model.getUiDataModel().getTxtService().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, FDR06020012, "", null);
            } else if (model.getUiDataModel().getTxtVslCode().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, FDR06020013, "", null);
            } else {

                SearchUtils su = new SearchUtils();
                mData = NFRUtils.getUserData();
                su.showTwoColSearch();
                su.setParameterValues(0, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtVslCode());
                su.setParameterValues(1, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtService());
               if (model.getStateModel().isChkmatchun()) {
            su.invokeTwoColSearch(FDRCBEAN, "060200033", event);
             }else
             {
               su.invokeTwoColSearch(FDRCBEAN, "06020003", event);   
             }

            }
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void btnPortActionPerformed(ActionEvent event) {
        try {
            if (model.getUiDataModel().getTxtService().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, FDR06020012, "", null);
            } else if (model.getUiDataModel().getTxtVslCode().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, FDR06020013, "", null);
            } else if (model.getUiDataModel().getTxtVoyageCode().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020014", "", null);
            } else {

                SearchUtils su = new SearchUtils();
                mData = NFRUtils.getUserData();
                su.showTwoColSearch();
                 if (model.getStateModel().isChkmatchun()) {
                su.setParameterValues(0, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtVslCode());
                su.setParameterValues(1, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtVoyageCode());
                su.setParameterValues(2, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtService());
                su.setParameterValues(3, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtBound().trim());
               
            su.invokeTwoColSearch(FDRCBEAN, "060200044", event);
             }else
             {
                 su.setParameterValues(0, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtVslCode());
                su.setParameterValues(1, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtVoyageCode());
                su.setParameterValues(2, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtService());
                su.setParameterValues(3, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtBound().trim());
               su.invokeTwoColSearch(FDRCBEAN, "06020004", event);   
             }
            }
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void btnterminalActionPerformed(ActionEvent event) {
        try {
            if (model.getUiDataModel().getTxtService().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, FDR06020012, "", null);
            } else if (model.getUiDataModel().getTxtVslCode().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, FDR06020013, "", null);
            } else if (model.getUiDataModel().getTxtVoyageCode().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020014", "", null);
            } else if (model.getUiDataModel().getTxtOpspod().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020015", "", null);
            } else {

                SearchUtils su = new SearchUtils();
                mData = NFRUtils.getUserData();
                su.showTwoColSearch();
              
                if (model.getStateModel().isChkmatchun()) {
                     su.setParameterValues(0, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtService());
                su.setParameterValues(1, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtVslCode());
                su.setParameterValues(2, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtVoyageCode());
                su.setParameterValues(3, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtBound().trim());
                su.setParameterValues(4, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtOpspod().trim());
            su.invokeTwoColSearch(FDRCBEAN, "060200055", event);
             }else
             {
                   su.setParameterValues(0, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtService());
                su.setParameterValues(1, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtVslCode());
                su.setParameterValues(2, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtVoyageCode());
                su.setParameterValues(3, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtBound().trim());
                su.setParameterValues(4, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtOpspod().trim());
               su.invokeTwoColSearch(FDRCBEAN, "06020005", event);   
             }
            }
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void btnNewSvcActionPerformed(ActionEvent event) {
        try {
            String forM = "";
            String qry = "SELECT Distinct serviceType FROM Service(NoLock) WHERE (serviceCode='" + model.getUiDataModel().getTxtService().trim() + "') and Status='Active' ";

            List ve = remote.getRecords(qry);
            Enumeration en = Collections.enumeration(ve);
            while (en.hasMoreElements()) {
                forM = en.nextElement().toString().trim();
            }
            SearchUtils su = new SearchUtils();
            mData = NFRUtils.getUserData();
            su.showTwoColSearch();
            if (FDRL.equalsIgnoreCase("true")) {
                if ("".equalsIgnoreCase(forM)) {

                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, "Active");
                    su.invokeTwoColSearch(FDRCBEAN, "6020022", event);
                } else {

                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, "Active");
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, forM.trim());
                    su.invokeTwoColSearch(FDRCBEAN, "6020023", event);
                }
            } else {
                if ("".equalsIgnoreCase(forM)) {

                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, "Active");
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, mData.getAgencyCode().trim());
                    su.invokeTwoColSearch(FDRCBEAN, "06020006", event);
                } else {

                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, "Active");
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, mData.getAgencyCode().trim());
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, forM.trim());
                    su.invokeTwoColSearch(FDRCBEAN, "06020007", event);
                }
            }
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void btnNewVslActionPerformed(ActionEvent event) {
        try {
            if (model.getUiDataModel().getTxtNewSvc().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, FDR06020017, "", null);
            } else {
                SearchUtils su = new SearchUtils();
                mData = NFRUtils.getUserData();
                if (FDRL.equalsIgnoreCase("true")) {
                    su.showTwoColSearch();
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, "A");
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewSvc().trim());
                    su.invokeTwoColSearch(FDRCBEAN, "6020024", event);
                } else {
                    su.showTwoColSearch();
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, "A");
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewSvc().trim());
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, mData.getAgencyCode().trim());
                    su.invokeTwoColSearch(FDRCBEAN, "06020008", event);
                }

            }
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void btnNewVoyBndActionPerformed(ActionEvent event) {
        try {
            if (model.getUiDataModel().getTxtNewSvc().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, FDR06020017, "", null);
            } else if (model.getUiDataModel().getTxtNewVslCode().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, FDR06020018, "", null);
            } else if (model.getUiDataModel().getTxtNewVslName().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, FDR06020019, "", null);
                model.getUiDataModel().setTxtNewVslName("");
            } else {
                SearchUtils su = new SearchUtils();
                mData = NFRUtils.getUserData();
                if (FDRL.equalsIgnoreCase("true")) {
                    su.showTwoColSearch();
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewSvc().trim());
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewVslCode().trim());
                    su.invokeTwoColSearch(FDRCBEAN, "6020025", event);
                } else {
                    su.showTwoColSearch();
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewSvc().trim());
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewVslCode().trim());
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, mData.getAgencyCode().trim());
                    su.invokeTwoColSearch(FDRCBEAN, "06020009", event);
                }

            }
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void btnNewOpspodActionPerformed(ActionEvent event) {
        try {
            if (model.getUiDataModel().getTxtNewSvc().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, FDR06020017, "", null);
            } else if (model.getUiDataModel().getTxtNewVslCode().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, FDR06020018, "", null);
            } else if (model.getUiDataModel().getTxtNewVslName().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, FDR06020019, "", null);
                model.getUiDataModel().setTxtNewVslName("");
            } else if (model.getUiDataModel().getTxtNewVoy().trim().length() == 0 || model.getUiDataModel().getTxtNewBnd().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, FDR06020020, "", null);
            } else {
                SearchUtils su = new SearchUtils();
                mData = NFRUtils.getUserData();
                if (FDRL.equalsIgnoreCase("true")) {
                    su.showTwoColSearch();
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, "A");
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewSvc().trim());
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewVslCode().trim());
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewVoy().trim());
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewBnd().trim());
                    su.invokeTwoColSearch(FDRCBEAN, "6020026", event);
                } else {
                    su.showTwoColSearch();
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, "A");
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewSvc().trim());
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewVslCode().trim());
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewVoy().trim());
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewBnd().trim());
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, mData.getAgencyCode().trim());
                    su.invokeTwoColSearch(FDRCBEAN, "06020010", event);
                }

            }
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void btnNewOpspodTerActionPerformed(ActionEvent event) {
        try {
            if (model.getUiDataModel().getTxtNewSvc().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, FDR06020017, "", null);
            } else if (model.getUiDataModel().getTxtNewVslCode().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, FDR06020018, "", null);
            } else if (model.getUiDataModel().getTxtNewVslName().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, FDR06020019, "", null);
                model.getUiDataModel().setTxtNewVslName("");
            } else if (model.getUiDataModel().getTxtNewVoy().trim().length() == 0 || model.getUiDataModel().getTxtNewBnd().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, FDR06020020, "", null);
            } else if (model.getUiDataModel().getTxtNewOpspod().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020021", "", null);
            } else {
                SearchUtils su = new SearchUtils();
                mData = NFRUtils.getUserData();
                if (FDRL.equalsIgnoreCase("true")) {
                    su.showTwoColSearch();
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, "A");
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewSvc().trim());
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewVslCode().trim());
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewVoy().trim());
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewBnd().trim());
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewOpspod().trim());
                    su.invokeTwoColSearch(FDRCBEAN, "6020027", event);
                } else {
                    su.showTwoColSearch();
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, "A");
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewSvc().trim());
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewVslCode().trim());
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewVoy().trim());
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewBnd().trim());
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewOpspod().trim());
                    su.setWhereCondition(SearchUtils.COND_EQUALTO, SearchUtils.DATATYPE_STRING, mData.getAgencyCode().trim());
                    su.invokeTwoColSearch(FDRCBEAN, "06020011", event);
                }

            }
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void btnShowActionPerformed(ActionEvent event) {
        try {

            OPTIONBUTTON = "";

            if ("".equalsIgnoreCase(model.getUiDataModel().getTxtService())) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, FDR06020012, "", null);
            } else if ("".equalsIgnoreCase(model.getUiDataModel().getTxtVslCode())) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, FDR06020013, "", null);
            } else if (model.getUiDataModel().getTxtVoyageCode().isEmpty()) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020030", "", null);
            } else if (model.getUiDataModel().getTxtBound().isEmpty()) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020031", "", null);
            } else if (model.getUiDataModel().getTxtOpspod().isEmpty()) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020032", "", null);
            } else if (model.getUiDataModel().getTxtOpspod().startsWith("ZF")) {
                model.getStateModel().setTxtterminalDis(true);
                model.getStateModel().setBtnterminalDis(true);
//                model.getStateModel().setChkmatch(true);
//                model.getStateModel().setChkmatchdis(true);
//                model.getUiDataModel().setGridStyle("sm-margin-right2 sm-table-wrap sm-border");
//                model.getUiDataModel().setTable1size("ui-g-12 ui-lg-9 ui-md-12 sm-padding0 sm-margin-top0 sm-input-size84");
//                model.getUiDataModel().setTable2size("ui-g-12 ui-lg-3 ui-md-12 sm-padding0 sm-margin-top0 sm-input-size16");
                useThreadToShow();
            } else {
                model.getStateModel().setTxtterminalDis(false);
                model.getStateModel().setBtnterminalDis(false);
//                model.getStateModel().setChkmatch(true);
//                model.getStateModel().setChkmatchdis(true);
//                model.getUiDataModel().setGridStyle("sm-margin-right2 sm-table-wrap sm-border");
//                model.getUiDataModel().setTable1size("ui-g-12 ui-lg-9 ui-md-12 sm-padding0 sm-margin-top0 sm-input-size84");
//                model.getUiDataModel().setTable2size("ui-g-12 ui-lg-3 ui-md-12 sm-padding0 sm-margin-top0 sm-input-size16");
                if (model.getUiDataModel().getTxtterminal().isEmpty()) {

                    dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020016", "", null);
                } else {

                    useThreadToShow();
                }
            }
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void useThreadToShow() {
        try {
            disableTheComp();
            showBtnAction();
            enableTheComp();
            if (!model.getUiDataModel().getfDGridList().isEmpty() && !model.getStateModel().isChkmatch()) {
                model.getStateModel().setButtonGroup2Dis(true);
                model.getUiDataModel().setGridStyle("sm-table-wrap sm-border");
                model.getUiDataModel().setTable1size("ui-g-12 ui-lg-9 ui-md-12 sm-padding0 sm-margin-top0 sm-input-size100");
                model.getUiDataModel().setTable2size("ui-g-12 ui-lg-3 ui-md-12 sm-padding0 sm-margin-top0 sm-input-size0");

            }
            refreshResultTable(TABLE2);
            refreshResultTable(TABLE1);
            RequestContext.getCurrentInstance().update("FDR-FDR_FRAGBOTHTABLE");
            RequestContext.getCurrentInstance().update(FRAGC);
            RequestContext.getCurrentInstance().update(FRAGB);
            RequestContext.getCurrentInstance().update(FRAGB1);
            RequestContext.getCurrentInstance().update(FRAG1);
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void showBtnAction() {

        try {

            model.getUiDataModel().setTxtOpspodNew("");
            model.getUiDataModel().setTxtterminalNew("");

            showGrid();
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void showGrid() {
        try {
             if (model.getStateModel().isChkmatchun()) {
            String service = model.getUiDataModel().getTxtService();
            String vessel = model.getUiDataModel().getTxtVslCode();
            String voyage = model.getUiDataModel().getTxtVoyageCode();
            String bound = model.getUiDataModel().getTxtBound();
            String pod = model.getUiDataModel().getTxtOpspod();
            String ter = model.getUiDataModel().getTxtterminal();
            String type;
            String option="up";
            model.getUiDataModel().setButtonGroup2(BKGNO);
            if (BKGNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {
                Map hmlagk;

                type = "BookNo";

                Object opObj = remote.getdetail2(service, vessel, voyage, bound, pod, ter, type, option);
                String retVals = String.valueOf(opObj);
                if ("No Records".equalsIgnoreCase(retVals.trim())) {
                    log.info("NO RECORDS >>>> ");
                } else {

                    Object obj = (Object) new solverminds.compress.CompressAPI().decompress(opObj);
                    hmlagk = (HashMap) obj;
                    List la = (ArrayList) hmlagk.get("Data");

                    model.getUiDataModel().setfDGridList(la);
                    model.getStateModel().setSelectenableDis(false);
                    if (!model.getUiDataModel().getfDGridList().isEmpty()) {

                        String callOrder = (String) hmlagk.get("CallOrder");
                        callOrder = (callOrder != null) ? callOrder.trim() : "0";
                        model.getUiDataModel().setTxtCallOrder(callOrder.trim());
                        model.getStateModel().setSetSvcDet(false);
                        model.getUiDataModel().setMatchingGridList(new ArrayList());
                    }
                }

                model.getStateModel().setGridhdr1(true);
                model.getStateModel().setGridhdr2(false);
                model.getUiDataModel().setBookcntrno("Book No / WO No");
                model.getUiDataModel().setLblStats("~ Matched Book No / WO No");
                model.getUiDataModel().setLblStats1("~ Unmatched Book No / WO No");
                List = BKGNUMBER;
                model.getUiDataModel().setTxtNewSvc("");
                model.getUiDataModel().setTxtNewVslCode("");
                model.getUiDataModel().setTxtNewVslName("");
                model.getUiDataModel().setTxtNewVoy("");
                model.getUiDataModel().setTxtNewBnd("");
                model.getUiDataModel().setTxtNewOpspod("");
                model.getUiDataModel().setTxtNewOpspodTer("");
                model.getStateModel().setChkmatchdis(false);

            } else if (BKGNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {
                Map hmlagk;

                type = "Workorder";

                Object opObj = remote.getdetail2(service, vessel, voyage, bound, pod, ter, type,option);
                String retVals = String.valueOf(opObj);
                if ("No Records".equalsIgnoreCase(retVals.trim())) {
                    log.info("NO RECORDS >>>> ");
                } else {
                    Object obj = (Object) new solverminds.compress.CompressAPI().decompress(opObj);
                    hmlagk = (HashMap) obj;
                    List la = (ArrayList) hmlagk.get("Data");

                    model.getUiDataModel().setfDGridList(la);
                    model.getStateModel().setSelectenableDis(false);
                    if (!model.getUiDataModel().getfDGridList().isEmpty()) {

                        String callOrder = (String) hmlagk.get("CallOrder");
                        callOrder = (callOrder != null) ? callOrder.trim() : "0";
                        model.getUiDataModel().setTxtCallOrder(callOrder.trim());
                        model.getUiDataModel().setMatchingGridList(new ArrayList());
                    }
                }

                List = "WO Number";
                model.getUiDataModel().setTxtNewSvc("");
                model.getUiDataModel().setTxtNewVslCode("");
                model.getUiDataModel().setTxtNewVslName("");
                model.getUiDataModel().setTxtNewVoy("");
                model.getUiDataModel().setTxtNewBnd("");
                model.getUiDataModel().setTxtNewOpspod("");
                model.getUiDataModel().setTxtNewOpspodTer("");

            }}
             else{
                         String service = model.getUiDataModel().getTxtService();
            String vessel = model.getUiDataModel().getTxtVslCode();
            String voyage = model.getUiDataModel().getTxtVoyageCode();
            String bound = model.getUiDataModel().getTxtBound();
            String pod = model.getUiDataModel().getTxtOpspod();
            String ter = model.getUiDataModel().getTxtterminal();
            String type;
            String option="lp";
            model.getUiDataModel().setButtonGroup2(BKGNO);
            if (BKGNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {
                Map hmlagk;

                type = "BookNo";

                Object opObj = remote.getdetail(service, vessel, voyage, bound, pod, ter, type, option);
                String retVals = String.valueOf(opObj);
                if ("No Records".equalsIgnoreCase(retVals.trim())) {
                    log.info("NO RECORDS >>>> ");
                } else {

                    Object obj = (Object) new solverminds.compress.CompressAPI().decompress(opObj);
                    hmlagk = (HashMap) obj;
                    List la = (ArrayList) hmlagk.get("Data");

                    model.getUiDataModel().setfDGridList(la);
                    model.getStateModel().setSelectenableDis(false);
                    if (!model.getUiDataModel().getfDGridList().isEmpty()) {

                        String callOrder = (String) hmlagk.get("CallOrder");
                        callOrder = (callOrder != null) ? callOrder.trim() : "0";
                        model.getUiDataModel().setTxtCallOrder(callOrder.trim());
                        model.getStateModel().setSetSvcDet(false);
                        model.getUiDataModel().setMatchingGridList(new ArrayList());
                    }
                }

                model.getStateModel().setGridhdr1(true);
                model.getStateModel().setGridhdr2(false);
                model.getUiDataModel().setBookcntrno("Book No / WO No");
                model.getUiDataModel().setLblStats("~ Matched Book No / WO No");
                model.getUiDataModel().setLblStats1("~ Unmatched Book No / WO No");
                List = BKGNUMBER;
                model.getUiDataModel().setTxtNewSvc("");
                model.getUiDataModel().setTxtNewVslCode("");
                model.getUiDataModel().setTxtNewVslName("");
                model.getUiDataModel().setTxtNewVoy("");
                model.getUiDataModel().setTxtNewBnd("");
                model.getUiDataModel().setTxtNewOpspod("");
                model.getUiDataModel().setTxtNewOpspodTer("");
                model.getStateModel().setChkmatchdis(false);

            } else if (BKGNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {
                Map hmlagk;

                type = "Workorder";

                Object opObj = remote.getdetail(service, vessel, voyage, bound, pod, ter, type,option);
                String retVals = String.valueOf(opObj);
                if ("No Records".equalsIgnoreCase(retVals.trim())) {
                    log.info("NO RECORDS >>>> ");
                } else {
                    Object obj = (Object) new solverminds.compress.CompressAPI().decompress(opObj);
                    hmlagk = (HashMap) obj;
                    List la = (ArrayList) hmlagk.get("Data");

                    model.getUiDataModel().setfDGridList(la);
                    model.getStateModel().setSelectenableDis(false);
                    if (!model.getUiDataModel().getfDGridList().isEmpty()) {

                        String callOrder = (String) hmlagk.get("CallOrder");
                        callOrder = (callOrder != null) ? callOrder.trim() : "0";
                        model.getUiDataModel().setTxtCallOrder(callOrder.trim());
                        model.getUiDataModel().setMatchingGridList(new ArrayList());
                    }
                }

                List = "WO Number";
                model.getUiDataModel().setTxtNewSvc("");
                model.getUiDataModel().setTxtNewVslCode("");
                model.getUiDataModel().setTxtNewVslName("");
                model.getUiDataModel().setTxtNewVoy("");
                model.getUiDataModel().setTxtNewBnd("");
                model.getUiDataModel().setTxtNewOpspod("");
                model.getUiDataModel().setTxtNewOpspodTer("");

            }}
             
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void svcRefreshActionPerformed(ActionEvent event) {
        try {
            model.getUiDataModel().setTxtNewSvc("");
            model.getUiDataModel().setTxtNewVslCode("");
            model.getUiDataModel().setTxtNewVslName("");
            model.getUiDataModel().setTxtNewVoy("");
            model.getUiDataModel().setTxtNewBnd("");
            model.getUiDataModel().setTxtNewOpspod("");
            model.getUiDataModel().setTxtNewOpspodTer("");
            model.getStateModel().setSetSvcDet(false);
            model.getStateModel().setBtnNewSvcDis(false);
            model.getStateModel().setNewSerRO(false);
            model.getStateModel().setBtnNewVslDis(false);
            model.getStateModel().setBtnNewVoyBndDis(false);
            model.getStateModel().setSetSvcDet(false);

            RequestContext.getCurrentInstance().update(FRAGB);
            RequestContext.getCurrentInstance().update(FRAGB1);
            RequestContext.getCurrentInstance().update(FRAGC);
        } catch (Exception e) {
            log.info(SOUT);

            log.fatal(e);
        }
    }

    public void setDefaultEmptyRow() {
        try {
            FDRmatchTableVo mvo = new FDRmatchTableVo();
            mvo.setContainerNo("");
            mvo.setBkNo("");
          mvo.setVinno("");
            mvo.setColor("NO");

            ArrayList la = new ArrayList();
            la.add(mvo);
            model.getUiDataModel().setMatchingGridList(la);
            RequestContext.getCurrentInstance().update(TABLE2);
        } catch (Exception e) {
            log.info(SOUT);

            log.fatal(e);
        }
    }

    public void btnErrorActionPerformed(ActionEvent evt) {
        try {

            boolean gridval = false;
            boolean lt = true;

            model.getUiDataModel().setSelectedGridValue(new ArrayList());
            model.getStateModel().setSelectenable(false);
            FDRforcedDischargeVO vos;
            for (int i = 0; i < model.getUiDataModel().getfDGridList().size(); i++) {
                vos = (FDRforcedDischargeVO) model.getUiDataModel().getfDGridList().get(i);
                vos.setSelectL(false);
            }

            FDRmatchTableVo vo;

            int nullchk = 0;
            for (int i = 0; i < model.getUiDataModel().getMatchingGridList().size(); i++) {
                vo = (FDRmatchTableVo) model.getUiDataModel().getMatchingGridList().get(i);

                if (BKGNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2()) && vo.getBkNo() != null && vo.getBkNo().length() > 0) {

                    nullchk++;

                } else if (CNTNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2()) && vo.getContainerNo() != null && vo.getContainerNo().length() > 0) {

                    nullchk++;

                }else if (VINNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2()) && vo.getContainerNo()!= null && vo.getContainerNo().length() > 0) {

                    nullchk++;

                }

            }

            if (BKGNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {
                List = BKGNUMBER;
            } else if (CNTNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {
                List = CNTNUMBER;
            }else {
                log.info("inside vinno"+VINNO);
                List = VINNO;
            }

            nullchked(nullchk, gridval, lt);
            refreshResultTable(TABLE1);
            refreshResultTable(TABLE2);
            RequestContext.getCurrentInstance().update(TABLE1);
            RequestContext.getCurrentInstance().update(TABLE2);
        } catch (Exception e) {
            log.info(SOUT);

            log.fatal(e);
        }
    }

    private void nullchked(int nullchk, boolean gridval, boolean lt) {
        FDRmatchTableVo vo;
        if (nullchk == 0) {
            if (BKGNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020035", "", null);
            } else if (CNTNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020036", "", null);
            }
            else if (VINNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, "060200366", "", null);
            }

        } else {

            for (int i = 0; i < model.getUiDataModel().getMatchingGridList().size(); i++) {
                vo = (FDRmatchTableVo) model.getUiDataModel().getMatchingGridList().get(i);
                if ("".equalsIgnoreCase(vo.getBkNo()) && "".equalsIgnoreCase(vo.getContainerNo())) {
                    gridval = false;
                    break;
                } 
                gridval = true;
            }
            if (gridval) {
                matchingFunction();
                sortbymatch(model.getUiDataModel().getfDGridList(), model.getUiDataModel().getMatchingGridList());

            }

            String s = model.getUiDataModel().getMatchingGridList().toString();
            s = (s != null) ? s.trim() : "";

            if ((BKGNUMBER.equalsIgnoreCase(List.trim())) && (s.length() == 0)) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020035", "", null);
            } else if ((CNTNUMBER.equalsIgnoreCase(List.trim())) && (s.length() == 0)) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020036", "", null);
            } 
             else if ((VINNO.equalsIgnoreCase(List.trim())) && (s.length() == 0)) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, "060200366", "", null);
             }else if (lt) {
                checkcon();
                sortbymatch(model.getUiDataModel().getfDGridList(), model.getUiDataModel().getMatchingGridList());
            }

        }
    }

    private void sortbymatch(List a1, List a2) {

        try {
            log.info("inside sorting >>> ");
            Collections.sort(a1, new Comparator<FDRforcedDischargeVO>() {

                @Override
                public int compare(FDRforcedDischargeVO o1, FDRforcedDischargeVO o2) {
                    return o1.getMatchSysL().compareTo(o2.getMatchSysL());
                }

            });

            Collections.sort(a2, new Comparator<FDRmatchTableVo>() {

                @Override
                public int compare(FDRmatchTableVo o1, FDRmatchTableVo o2) {
                    return o1.getBgcolor().compareTo(o2.getBgcolor());
                }

            });

        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }

    }

    private void matchingFunction() {
        try {
            HashSet matchingvalueset = new HashSet();
            HashSet matchedvalueset = new HashSet();
            //            log.info(new StringBuilder().append("list size ").append(model.getUiDataModel().getMatchingGridList().size()).toString());
            String bkcno;
            FDRforcedDischargeVO lcvo;

            for (int i = 0; i < model.getUiDataModel().getfDGridList().size(); i++) {
                lcvo = (FDRforcedDischargeVO) model.getUiDataModel().getfDGridList().get(i);
                lcvo.setSelectL(false);
                lcvo.setColor(lcvo.getOpsStatusL().trim());
            }
            if (BKGNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {
                bkcno = "bkno";
            } else  if (VINNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {
                bkcno = "vno";
            } else
            {
                     bkcno = "cno";
                    }
            boolean matchsts = false;
            boolean ensureset = false;
            matchsts = loop1(bkcno, matchingvalueset, matchedvalueset, matchsts, ensureset);

            if (!matchsts) {
                if (BKGNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {

                    dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020038", "", null);
                } else  if (CNTNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {

                    dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020039", "", null);
                } else
                {
                     dutil.showDialog(FDR, SMDialogUtil.type.INFO, "60200399", "", null);
                }
            }
            RequestContext.getCurrentInstance().update(TABLE1);
            RequestContext.getCurrentInstance().update(TABLE2);

        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    private boolean loop1(String bkcno, HashSet matchingvalueset, HashSet matchedvalueset, boolean matchsts, boolean ensureset) {

        if (("cno").equalsIgnoreCase(bkcno)) {
            matchsts = contno(matchingvalueset, matchedvalueset, matchsts, ensureset);
        } else  if (("bkno").equalsIgnoreCase(bkcno)) {
            matchsts = bookno(matchingvalueset, matchedvalueset, matchsts, ensureset);
        }
        else
        {
             matchsts = vinno(matchingvalueset, matchedvalueset, matchsts, ensureset);
        }
        return matchsts;
    }
    private boolean contno(HashSet matchingvalueset, HashSet matchedvalueset, boolean matchsts, boolean ensureset) {
        FDRmatchTableVo mcvo;
        FDRforcedDischargeVO lcvo;
        for (int i = 0; i < model.getUiDataModel().getMatchingGridList().size(); i++) {

            mcvo = (FDRmatchTableVo) model.getUiDataModel().getMatchingGridList().get(i);
            matchingvalueset.add(mcvo.getContainerNo().trim().toUpperCase());
        }
        for (int j = 0; j < model.getUiDataModel().getfDGridList().size(); j++) {

            lcvo = (FDRforcedDischargeVO) model.getUiDataModel().getfDGridList().get(j);
            if (matchingvalueset.contains(lcvo.getContainerNoL().trim())) {
                lcvo.setSelectL(true);

                lcvo.setMatchSysL(MATCH);

                matchedvalueset.add(lcvo.getContainerNoL().trim().toUpperCase());
                matchsts = true;

                if (!ensureset) {
                    ensureset = true;
                }
            }
            model.getUiDataModel().getfDGridList().set(j, lcvo);
        }
        for (int i = 0; i < model.getUiDataModel().getMatchingGridList().size(); i++) {

            mcvo = (FDRmatchTableVo) model.getUiDataModel().getMatchingGridList().get(i);
            if (matchedvalueset.contains(mcvo.getContainerNo().trim().toUpperCase())) {
                mcvo.setBgcolor("MatchedColor");
            } else {
                mcvo.setBgcolor("Unmatched");
            }
            model.getUiDataModel().getMatchingGridList().set(i, mcvo);
        }
        return matchsts;
    }
     private boolean vinno(HashSet matchingvalueset, HashSet matchedvalueset, boolean matchsts, boolean ensureset) {
        FDRmatchTableVo mcvo;
        FDRforcedDischargeVO lcvo;
        for (int i = 0; i < model.getUiDataModel().getMatchingGridList().size(); i++) {

            mcvo = (FDRmatchTableVo) model.getUiDataModel().getMatchingGridList().get(i);
            matchingvalueset.add(mcvo.getContainerNo().trim().toUpperCase());
        }
        for (int j = 0; j < model.getUiDataModel().getfDGridList().size(); j++) {

            lcvo = (FDRforcedDischargeVO) model.getUiDataModel().getfDGridList().get(j);
            if (matchingvalueset.contains(lcvo.getVinchassis().trim())) {
                lcvo.setSelectL(true);

                lcvo.setMatchSysL(MATCH);

                matchedvalueset.add(lcvo.getVinchassis().trim().toUpperCase());
                matchsts = true;

                if (!ensureset) {
                    ensureset = true;
                }
            }
            model.getUiDataModel().getfDGridList().set(j, lcvo);
        }
        for (int i = 0; i < model.getUiDataModel().getMatchingGridList().size(); i++) {

            mcvo = (FDRmatchTableVo) model.getUiDataModel().getMatchingGridList().get(i);
            if (matchedvalueset.contains(mcvo.getContainerNo().trim().toUpperCase())) {
                mcvo.setBgcolor("MatchedColor");
            } else {
                mcvo.setBgcolor("Unmatched");
            }
            model.getUiDataModel().getMatchingGridList().set(i, mcvo);
        }
        return matchsts;
    }
    private boolean bookno(HashSet matchingvalueset, HashSet matchedvalueset, boolean matchsts, boolean ensureset) {
        FDRmatchTableVo mcvo;
        FDRforcedDischargeVO lcvo;
        for (int i = 0; i < model.getUiDataModel().getMatchingGridList().size(); i++) {

            mcvo = (FDRmatchTableVo) model.getUiDataModel().getMatchingGridList().get(i);

            matchingvalueset.add(mcvo.getBkNo().trim().toUpperCase());
        }
        for (int j = 0; j < model.getUiDataModel().getfDGridList().size(); j++) {

            lcvo = (FDRforcedDischargeVO) model.getUiDataModel().getfDGridList().get(j);
            if (matchingvalueset.contains(lcvo.getBookNoL().trim())) {
                lcvo.setSelectL(true);

                lcvo.setMatchSysL(MATCH);
                matchedvalueset.add(lcvo.getBookNoL().trim().toUpperCase());
                matchsts = true;
                if (!ensureset) {
                    ensureset = true;
                }
            }
            model.getUiDataModel().getfDGridList().set(j, lcvo);
        }
        for (int i = 0; i < model.getUiDataModel().getMatchingGridList().size(); i++) {

            mcvo = (FDRmatchTableVo) model.getUiDataModel().getMatchingGridList().get(i);
            if (matchedvalueset.contains(mcvo.getBkNo().trim().toUpperCase())) {
                mcvo.setBgcolor("MatchedColor");
            } else {
                mcvo.setBgcolor("Unmatched");
            }
            model.getUiDataModel().getMatchingGridList().set(i, mcvo);
        }
        return matchsts;
    }
    public void radiotypechange() {
        String temp = model.getUiDataModel().getButtonGroup2().trim();

        try {
            String ttmp = "";
            if (!OPTIONBUTTON.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {
                if (BKGNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {
                    model.getStateModel().setGridhdr1(true);
                    model.getStateModel().setGridhdr2(false);

                    model.getUiDataModel().setBookcntrno(BKGNO);
                    model.getUiDataModel().setLblStats("~ Matched Book No / WO No");
                    model.getUiDataModel().setLblStats1("~ Unmatched Book No / WO No");

                    int rowcnt = model.getUiDataModel().getfDGridList().size();

                    radiobkgno(rowcnt, ttmp);

                } else if (CNTNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {
                    model.getStateModel().setGridhdr1(false);
                    model.getStateModel().setGridhdr2(true);

                    model.getUiDataModel().setBookcntrno(CNTNUMBER);
                    model.getUiDataModel().setLblStats("~ Matched Container No");
                    model.getUiDataModel().setLblStats1("~ Unmatched Container No");

                    int rowcnt = model.getUiDataModel().getfDGridList().size();

                    radiocontno(rowcnt, ttmp);

                }else if (VINNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {
                 
                    model.getStateModel().setGridhdr1(false);
                    model.getStateModel().setGridhdr2(true);

                    model.getUiDataModel().setBookcntrno(VINNO);
                    model.getUiDataModel().setLblStats("~ Matched VIN/Chassis No ");
                    model.getUiDataModel().setLblStats1("~ Unmatched VIN/Chassis No ");

                    int rowcnt = model.getUiDataModel().getfDGridList().size();

                    radiovinno(rowcnt, ttmp);

                }
                model.getUiDataModel().setSelectedGridValue(new ArrayList());
                model.getStateModel().setSelectenable(false);
                OPTIONBUTTON = "";
                refreshResultTable(TABLE1);
                refreshResultTable(TABLE2);
                FDRforcedDischargeVO vo;
                for (int i = 0; i < model.getUiDataModel().getfDGridList().size(); i++) {
                    vo = (FDRforcedDischargeVO) model.getUiDataModel().getfDGridList().get(i);
                    vo.setMatchSysL("");
                    vo.setSelectL(false);
                }
                RequestContext.getCurrentInstance().update(TABLE1);
                RequestContext.getCurrentInstance().update(TABLE2);
                RequestContext.getCurrentInstance().update("FDR-FDR_FRAGL");
            } else {
                //            log.info("Same Option FDR >> ");
                RequestContext.getCurrentInstance().update("FDR-FDR_buttonGroup2");

            }
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }
    private void radiocontno(int rowcnt, String ttmp) {
        FDRforcedDischargeVO cvo;
        List tm = (ArrayList) model.getUiDataModel().getfDGridList();
        for (int i = 0; i < rowcnt; i++) {

            cvo = (FDRforcedDischargeVO) tm.get(i);
            ttmp = cvo.getContainerNoL();
        }
        FDRforcedDischargeVO fvo;
        List ftm = (ArrayList) model.getUiDataModel().getfDGridList();
        if (ttmp.length() > 0) {
            for (int i = 0; i < rowcnt; i++) {
                fvo = (FDRforcedDischargeVO) ftm.get(i);
                fvo.setSelectL(false);
                fvo.setMatchSysL("N");
            }
        }
        if (!model.getUiDataModel().getfDGridList().isEmpty()) {
            setDefaultEmptyRow();
        }
        for (int l = 0; l < model.getUiDataModel().getfDGridList().size(); l++) {
            fvo = (FDRforcedDischargeVO) ftm.get(l);
            fvo.setSelectL(false);
            fvo.setMatchSysL("N");
        }
    }
    private void radiobkgno(int rowcnt, String ttmp) {
        FDRforcedDischargeVO cvo;
        List tm = (ArrayList) model.getUiDataModel().getfDGridList();
        for (int i = 0; i < rowcnt; i++) {
            cvo = (FDRforcedDischargeVO) tm.get(i);
            ttmp = cvo.getBookNoL();
        }
        FDRforcedDischargeVO fvo;
        List ftm = (ArrayList) model.getUiDataModel().getfDGridList();
        if (ttmp.length() > 0) {
            for (int i = 0; i < rowcnt; i++) {
                fvo = (FDRforcedDischargeVO) ftm.get(i);
                fvo.setSelectL(false);
                fvo.setMatchSysL("N");
            }
        }
        if (!model.getUiDataModel().getfDGridList().isEmpty()) {
            setDefaultEmptyRow();
        }
        for (int l = 0; l < model.getUiDataModel().getfDGridList().size(); l++) {
            fvo = (FDRforcedDischargeVO) ftm.get(l);
            fvo.setSelectL(false);
            fvo.setMatchSysL("N");
        }
    }
    private void radiovinno(int rowcnt, String ttmp) {
        FDRforcedDischargeVO cvo;
        List tm = (ArrayList) model.getUiDataModel().getfDGridList();
        for (int i = 0; i < rowcnt; i++) {
            cvo = (FDRforcedDischargeVO) tm.get(i);
            ttmp = cvo.getVinchassis();
        }
        FDRforcedDischargeVO fvo;
        List ftm = (ArrayList) model.getUiDataModel().getfDGridList();
        if (ttmp.length() > 0) {
            for (int i = 0; i < rowcnt; i++) {
                fvo = (FDRforcedDischargeVO) ftm.get(i);
                fvo.setSelectL(false);
                fvo.setMatchSysL("N");
            }
        }
        if (!model.getUiDataModel().getfDGridList().isEmpty()) {
            setDefaultEmptyRow();
        }
        for (int l = 0; l < model.getUiDataModel().getfDGridList().size(); l++) {
            fvo = (FDRforcedDischargeVO) ftm.get(l);
            fvo.setSelectL(false);
            fvo.setMatchSysL("N");
        }
    }
    public void addrows() {
        try {
            model.getUiDataModel().setSelectedGridValue(new ArrayList());
            model.getStateModel().setSelectenable(false);
            List alist = new ArrayList();
            List ob = new ArrayList();
            if ((BKGNO).equalsIgnoreCase(model.getUiDataModel().getButtonGroup2().trim())) {
                 addbkgno(ob, alist);
                addcontno(ob, alist);
           
            } else {
                 addcontno(ob, alist);
            }
            FDRforcedDischargeVO vos;
            for (int i = 0; i < model.getUiDataModel().getfDGridList().size(); i++) {
                vos = (FDRforcedDischargeVO) model.getUiDataModel().getfDGridList().get(i);
                vos.setMatchSysL("");
                vos.setSelectL(false);
            }
             log.info("gridsize beforer refredhmatchgrid$$$$$>>>>>>>>>>"+model.getUiDataModel().getMatchingGridList().size());
            log.info("gridsize before refredh>>>>>>>>>>"+model.getUiDataModel().getfDGridList().size());
            refreshResultTable(TABLE2);
              refreshResultTable(TABLE1);
               log.info("gridsize after refredh>>>>>>>>>>"+model.getUiDataModel().getfDGridList().size());
             log.info("gridsize after refredhmatchgrid$$$$$>>>>>>>>>>"+model.getUiDataModel().getMatchingGridList().size());
            RequestContext.getCurrentInstance().update(TABLE2);
            RequestContext.getCurrentInstance().update(TABLE1);
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }
      
    private void addbkgno(List ob, List alist) {
        FDRmatchTableVo vo;
        if (!model.getUiDataModel().getMatchingGridList().isEmpty()) {
            for (int l = 0; l < model.getUiDataModel().getMatchingGridList().size(); l++) {
                vo = (FDRmatchTableVo) model.getUiDataModel().getMatchingGridList().get(l);
                String[] bkNos = vo.getBkNo().split(" ");
                  //log.info("vinno is>>>>>>>>>>>>>>> "+vo.getBkNo());
                for (String bkNo1 : bkNos) {
                    if (ob.contains(bkNo1)) {
                    } else if (!bkNo1.trim().isEmpty()) {
                        ob.add(bkNo1);
                    }
                }
            }
            for (Object ob1 : ob) {
                vo = new FDRmatchTableVo();
                vo.setBkNo((String) ob1);
                vo.setColor("");
                vo.setContainerNo("");
                alist.add(vo);
            }
            if (ob.isEmpty()) {
               setDefaultEmptyRow();
            }
            model.getUiDataModel().setMatchingGridList((ArrayList) alist);
        }
    }
    private void addcontno(List ob, List alist) {
        FDRmatchTableVo vo;
        if (!model.getUiDataModel().getMatchingGridList().isEmpty()) {
            for (int l = 0; l < model.getUiDataModel().getMatchingGridList().size(); l++) {
                vo = (FDRmatchTableVo) model.getUiDataModel().getMatchingGridList().get(l);
                String[] containerNos = vo.getContainerNo().split(" ");
                for (String containerNo1 : containerNos) {
                    if (ob.contains(containerNo1)) {
                    } else if (!containerNo1.trim().isEmpty()) {
                        ob.add(containerNo1);
                    }
                }
            }
            for (Object ob1 : ob) {
                vo = new FDRmatchTableVo();
                vo.setContainerNo((String) ob1);
                vo.setColor("");
                vo.setBkNo("");
                alist.add(vo);
            }
            
            if (ob.isEmpty()) {
                setDefaultEmptyRow();
            }

            model.getUiDataModel().setMatchingGridList((ArrayList) alist);
        }
    }

    public void checkcon() {
        try {

            String temp;

            int s = model.getUiDataModel().getMatchingGridList().size();
            int r = model.getUiDataModel().getfDGridList().size();

            FDRforcedDischargeVO fvo = null;

            List ftm = (ArrayList) model.getUiDataModel().getfDGridList();
            List mtm = (ArrayList) model.getUiDataModel().getMatchingGridList();

            FDRmatchTableVo mvo;
            for (int b = 0; b < r; b++) {

                fvo = (FDRforcedDischargeVO) ftm.get(b);
                for (int j = 0; j < s; j++) {

                    mvo = (FDRmatchTableVo) mtm.get(j);
                    String conVal;
                    if (mvo.getBkNo() != null && mvo.getBkNo().toString().length() > 0) {

                        conVal = mvo.getBkNo();
                    } 
                    else{
                        conVal = mvo.getContainerNo();
                    }
                    conVal = (conVal != null) ? conVal.trim() : "";

                    if (CNTNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {
                        temp = fvo.getContainerNoL();

                        temp = (temp != null) ? temp.trim() : "";
                        if (conVal.equalsIgnoreCase(temp)) {

                            fvo.setSelectL(true);
                            fvo.setMatchSysL(MATCH);
                            mvo.setColor("Y");

                            break;
                        }
                        else {

                            fvo.setSelectL(false);
                            fvo.setMatchSysL("UnMatch");
                            mvo.setColor("N");

                        }
                    }  else if (VINNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {
                        temp = fvo.getVinchassis();

                        temp = (temp != null) ? temp.trim() : "";
                        if (conVal.equalsIgnoreCase(temp)) {

                            fvo.setSelectL(true);
                            fvo.setMatchSysL(MATCH);
                            mvo.setColor("Y");

                            break;
                             } else {
                            fvo.setSelectL(false);
                            fvo.setMatchSysL("UnMatch");
                            mvo.setColor("N");
                        }
                        }else if (BKGNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {
                        temp = fvo.getBookNoL();
                        temp = (temp != null) ? temp.trim() : "";

                        if (conVal.equalsIgnoreCase(temp)) {
                            fvo.setSelectL(true);
                            fvo.setMatchSysL(MATCH);
                            mvo.setColor("Y");
                            break;
                        } else {
                            fvo.setSelectL(false);
                            fvo.setMatchSysL("UnMatch");
                            mvo.setColor("N");
                        }
                    }
                }
            }

            if (BKGNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {
                List la = new ArrayList();
                List planModel = (ArrayList) model.getUiDataModel().getfDGridList();
                chkbkg(planModel, fvo, la, mtm);
            }
            else if (VINNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {
              List la = new ArrayList();
                List planModel = (ArrayList) model.getUiDataModel().getfDGridList();
                chkvinno(planModel, fvo, la, mtm);   
            }
                
                else {
                List la = new ArrayList();
                List planModel = (ArrayList) model.getUiDataModel().getfDGridList();
                chkcon(planModel, fvo, la, mtm);
            }

        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    private void chkcon(List planModel, FDRforcedDischargeVO fvo, List la, List mtm) {
        String tmp;
        FDRmatchTableVo mvo;
        for (Object planModel1 : planModel) {
            tmp = fvo.getMatchSysL();
            tmp = (tmp != null) ? tmp.trim() : "";
            fvo = (FDRforcedDischargeVO) planModel1;
            if (MATCH.equalsIgnoreCase(tmp)) {
                tmp = fvo.getContainerNoL();
                tmp = (tmp != null) ? tmp.trim() : "";
                if (tmp.length() > 0) {
                    la.add(tmp.trim().toUpperCase());
                }
            }
        }
        List planMdl = (ArrayList) model.getUiDataModel().getMatchingGridList();
        for (int l = 0; l < planMdl.size(); l++) {
            mvo = (FDRmatchTableVo) planMdl.get(l);
            mvo.setColor("N");
        }
        for (int l = 0; l < planMdl.size(); l++) {
            mvo = (FDRmatchTableVo) planMdl.get(l);
            tmp = mvo.getContainerNo();
            tmp = (tmp != null) ? tmp.trim() : "";
            if (la.contains(tmp.trim().toUpperCase())) {
                mvo = (FDRmatchTableVo) mtm.get(l);
                mvo.setColor("Y");
            }
        }
    }
    private void chkvinno(List planModel, FDRforcedDischargeVO fvo, List la, List mtm) {
        String tmp;
        FDRmatchTableVo mvo;
        for (Object planModel1 : planModel) {
            tmp = fvo.getMatchSysL();
            tmp = (tmp != null) ? tmp.trim() : "";
            fvo = (FDRforcedDischargeVO) planModel1;
            if (MATCH.equalsIgnoreCase(tmp)) {
                tmp = fvo.getVinchassis();
                tmp = (tmp != null) ? tmp.trim() : "";
                if (tmp.length() > 0) {
                    la.add(tmp.trim().toUpperCase());
                }
            }
        }
        List planMdl = (ArrayList) model.getUiDataModel().getMatchingGridList();
        for (int l = 0; l < planMdl.size(); l++) {
            mvo = (FDRmatchTableVo) planMdl.get(l);
            mvo.setColor("N");
        }
        for (int l = 0; l < planMdl.size(); l++) {
            mvo = (FDRmatchTableVo) planMdl.get(l);
            tmp = mvo.getContainerNo();
            tmp = (tmp != null) ? tmp.trim() : "";
            if (la.contains(tmp.trim().toUpperCase())) {
                mvo = (FDRmatchTableVo) mtm.get(l);
                mvo.setColor("Y");
            }
        }
    }

    private void chkbkg(List planModel, FDRforcedDischargeVO fvo, List la, List mtm) {
        String tmp;
        FDRmatchTableVo mvo;
        for (Object planModel1 : planModel) {
            tmp = fvo.getMatchSysL();
            tmp = (tmp != null) ? tmp.trim() : "";
            fvo = (FDRforcedDischargeVO) planModel1;
            if (MATCH.equalsIgnoreCase(tmp)) {
                tmp = fvo.getBookNoL();
                tmp = (tmp != null) ? tmp.trim() : "";
                if (tmp.length() > 0) {
                    la.add(tmp.trim().toUpperCase());
                }
            }
        }

        List planMdl = (ArrayList) model.getUiDataModel().getMatchingGridList();
        for (int l = 0; l < planMdl.size(); l++) {
            mvo = (FDRmatchTableVo) planMdl.get(l);
            mvo.setColor("N");
        }
        for (int l = 0; l < planMdl.size(); l++) {

            mvo = (FDRmatchTableVo) planMdl.get(l);
            tmp = mvo.getBkNo();
            tmp = (tmp != null) ? tmp.trim() : "";
            if (la.contains(tmp.trim().toUpperCase())) {
                mvo = (FDRmatchTableVo) mtm.get(l);
                mvo.setColor("Y");
            }
        }
    }

    public boolean validateimpdoc() {
        boolean status = true;
        boolean chkstatus = false;
        try {
            model.getUiDataModel().setImportlist(new ArrayList());
            FDRforcedDischargeVO vo;
            StringBuilder sb = new StringBuilder();
            List al = new ArrayList();
            int count = 0;
            FDRforcedDischargeVO vos;
            for (int l = 0; l < model.getUiDataModel().getfDGridList().size(); l++) {
                vo = (FDRforcedDischargeVO) model.getUiDataModel().getfDGridList().get(l);
                if (vo.getSelectL() && ("Y").equalsIgnoreCase(vo.getImpstatusL())) {
                    if (count == 0) {
                        al.add(vo.getBookNoL());
                        vo.setSelectL(false);
                    } else {
                        if (!al.contains(vo.getBookNoL())) {
                            al.add(vo.getBookNoL());
                        }
                        vo.setSelectL(false);
                        chkstatus = true;
                    }
                    status = false;
                    count++;
                    vos = new FDRforcedDischargeVO();
                    vos.setBookNoL(vo.getBookNoL().trim());
                    model.getUiDataModel().getImportlist().add(vos);
                }
            }

            for (int i = 0; i < al.size(); i++) {
                if (i == 0) {
                    sb.append(al.get(i));
                } else {
                    sb.append(", ").append(al.get(i));
                }
            }

            for (int l = 0; l < model.getUiDataModel().getfDGridList().size(); l++) {
                vo = (FDRforcedDischargeVO) model.getUiDataModel().getfDGridList().get(l);
                if (vo.getSelectL()) {
                    status = true;
                }
            }

            finalmsgs = sb.toString();
            if (!status) {
//                dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020040", "", null);
                log.info("inside imp doc validation >>>> ");
                model.getStateModel().setShowimppanel(true);
                model.getStateModel().setShowcontpanel(false);
                model.getStateModel().setSelectenable(false);
                RequestContext.getCurrentInstance().execute("PF('FDR_CV').show();");
                RequestContext.getCurrentInstance().update("FDR_Cont");
            } else if (chkstatus && status) {
                finalmsg = true;
            }
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
        return status;
    }

    public void btnConfActionPerformed(ActionEvent event) {
        try {
            int count = 0;
            List planModel = (ArrayList) model.getUiDataModel().getfDGridList();
            FDRforcedDischargeVO vo;
            for (Object planModel1 : planModel) {
                vo = (FDRforcedDischargeVO) planModel1;
                if ("true".equalsIgnoreCase(vo.getSelectL().toString())) {
                    count = count + 1;
                }
            }
            if (count >= 1 && model.getUiDataModel().getTxtNewOpspod().trim().length() > 0 && model.getUiDataModel().getTxtNewSvc().trim().length() > 0
                    && model.getUiDataModel().getTxtNewVslCode().trim().length() > 0 && model.getUiDataModel().getTxtNewVoy().trim().length() > 0
                    && model.getUiDataModel().getTxtNewBnd().trim().length() > 0 && (model.getUiDataModel().getTxtNewOpspodTer().trim().length() > 0)) {
                if (checkNextPlan() && checkLoadPortWithNewPOD() && validatevoyageclose()) {
                    if (validateimpdoc()) {
                        setData();
                    } else {
                        refreshResultTable(TABLE1);
                        RequestContext.getCurrentInstance().update(TABLE1);
                    }
                }
            } else {

                validation(count);
            }
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    private boolean validatevoyageclose() {
        boolean status = true;
        try {
            if ("true".equalsIgnoreCase(VLC)) {
                SearchUtils su = new SearchUtils();
                su.setParameterValues(0, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewSvc().trim());
                su.setParameterValues(1, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewVslCode().trim());
                su.setParameterValues(2, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewVoy().trim());
                su.setParameterValues(3, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewBnd().trim());
                su.setParameterValues(4, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewOpspod().trim());
                su.setParameterValues(5, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewOpspodTer().trim());
                su.setParameterValues(6, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewCallId().trim());
                String value = su.checkCode(FDR, "6020015");
                log.info("VOYAGE CLOSED FDR >> " + value);
                if (!"INVALID".equalsIgnoreCase(value) && "Y".equalsIgnoreCase(value)) {
                    dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020042", "", null);
                    status = false;
                }

            }

        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }

        return status;
    }

    private void validation(int count) {
        if (count == 0) {
            dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020027", "", null);
        } else if ((model.getUiDataModel().getTxtOpspodNew().trim().length() == 0) || (model.getUiDataModel().getTxtterminalNew().trim().length() == 0)) {
            if (model.getUiDataModel().getTxtNewSvc().trim().length() == 0) {
                dutil.showDialog(FDR, SMDialogUtil.type.INFO, FDR06020017, "", null);
            } else if (model.getUiDataModel().getTxtNewVslCode().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, FDR06020018, "", null);
            } else if (model.getUiDataModel().getTxtNewVoy().trim().length() == 0 || model.getUiDataModel().getTxtNewBnd().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, FDR06020020, "", null);
            } else if (model.getUiDataModel().getTxtNewOpspod().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020022", "", null);
            } else if (model.getUiDataModel().getTxtNewOpspodTer().trim().length() == 0) {

                dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020023", "", null);
            }
        }
    }

    public void removeFilterFDGrid() {

        model.getUiDataModel().getfDGridList().removeAll(model.getUiDataModel().getSelectedGridValue());
        RequestContext.getCurrentInstance().update(TABLE1);
    }

    public boolean checkNextPlan() {
        boolean retVal = false;
        try {
            if (TRANSHIPMENT.equalsIgnoreCase(model.getUiDataModel().getCmbPlanlist().toString())) {
                String tmp;
                String bkgNo;
                String cntNo;
                List alc = new ArrayList();
                String newOpspod = model.getUiDataModel().getTxtOpspodNew().trim();

                List planModel = (ArrayList) model.getUiDataModel().getfDGridList();
                FDRforcedDischargeVO vo;
                for (Object planModel1 : planModel) {
                    vo = (FDRforcedDischargeVO) planModel1;
                    if ("true".equalsIgnoreCase(vo.getSelectL().toString())) {
                        tmp = vo.getpLD().toString();
                        tmp = (tmp != null) ? tmp.trim() : "";

                        if (tmp.equalsIgnoreCase(newOpspod)) {
                            bkgNo = vo.getBookNoL();
                            bkgNo = (bkgNo != null) ? bkgNo.trim() : "";
                            if (model.getStateModel().isChkmatchun()) {
                                 cntNo = vo.getVinchassis();
                            }
                            else{
                             cntNo = vo.getContainerNoL();
                            }
                            cntNo = (cntNo != null) ? cntNo.trim() : "";
                            alc.add(bkgNo + "@@" + cntNo);
                        }

                    }
                }

                if (!alc.isEmpty()) {
                    retVal = false;

                    StringBuilder dispmsg = new StringBuilder();

                    dispmsg.append("<html><font face='Verdana' size='2' color=blue><B>The Following Book No(s) / WorkOrder No(s) and Container No(s) have an Delivery port : " + newOpspod + " </B></font><BR>");
                    dispmsg.append("<font face='Verdana' size='2' color=blue><B>Selected New POD and Delivery Port is Equal. Next Plan Should be Local.</B></font><BR>");

                    for (Object al_cont1 : alc) {
                        String[] d = al_cont1.toString().split("@@");
                        bkgNo = d[0];
                        bkgNo = bkgNo != null ? bkgNo.trim() : "";
                        cntNo = d[1];
                        cntNo = cntNo != null ? cntNo.trim() : "";
                        dispmsg.append("<font face='Verdana' size='2' color=blue><B>Book/WO No: " + bkgNo + "     Container No: " + cntNo + " </B></font><BR>");
                    }

                    String str = dispmsg.toString();
                    dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020033", "", new String[]{str});
                } else {
                    retVal = true;
                }
            } else {
                retVal = true;
            }
        } catch (Exception e) {
            log.info(SOUT);

            log.fatal(e);
        }
        return retVal;
    }

    public boolean checkLoadPortWithNewPOD() {
        boolean retValLa = true;
        try {
            String newPODLa = model.getUiDataModel().getTxtOpspodNew().trim();
            String newPODTerLa = model.getUiDataModel().getTxtterminalNew().trim();

            if (newPODLa.length() > 0 && newPODTerLa.length() > 0) {
                String tmpLa1;
                String tmpLa2;
                Set sLa = new TreeSet();

                List planModel = (ArrayList) model.getUiDataModel().getfDGridList();
                FDRforcedDischargeVO vo;
                for (Object planModel1 : planModel) {
                    vo = (FDRforcedDischargeVO) planModel1;
                    tmpLa1 = vo.getoPSPOLL();
                    tmpLa1 = (tmpLa1 != null) ? tmpLa1.trim() : "";
                    tmpLa2 = vo.getoPSPOLTerminalL();
                    tmpLa2 = (tmpLa2 != null) ? tmpLa2.trim() : "";
                    if (tmpLa1.equalsIgnoreCase(newPODLa) && tmpLa2.equalsIgnoreCase(newPODTerLa) && "true".equalsIgnoreCase(vo.getSelectL().toString())) {
                        tmpLa1 = vo.getBookNoL();
                        tmpLa1 = (tmpLa1 != null) ? tmpLa1.trim() : "";
                         if (model.getStateModel().isChkmatchun()) {
                            tmpLa2 = vo.getVinchassis();
                         }else{
                        tmpLa2 = vo.getContainerNoL();
                         }
                        tmpLa2 = (tmpLa2 != null) ? tmpLa2.trim() : "";
                        sLa.add(tmpLa1 + " ~~ " + tmpLa2);
                    }
                }

                if (!sLa.isEmpty()) {
                    retValLa = false;

                    StringBuffer dispmsg = new StringBuffer();
                    dispmsg.append("<html><font face='Verdana' size='2' color=blue><B>OPSPOL/OPSPOL Terminal and New POD/POD Terminal is Equal for following <BR>Containers.</B></font><BR>");
                    dispmsg.append("<font face='Verdana' size='2' color=blue><B>BookNo/WorkOrderNo ~~ ContainerNo</B></font><BR>");

                    Iterator itLa = sLa.iterator();
                    while (itLa.hasNext()) {
                        tmpLa1 = itLa.next().toString().trim();
                        dispmsg.append("<font face='Verdana' size='2' color=red>" + tmpLa1 + "<B></B></font><BR>");
                    }
                    dispmsg.append("<font face='Verdana' size='2' color=blue><B>It Should not be Equal.</B></font><BR>");

                    String str = dispmsg.toString();
                    dutil.showDialog(FDR, SMDialogUtil.type.INFO, str, "06020034", new String[]{str});
                }
            }

        } catch (Exception e) {
            log.info(SOUT);

            log.fatal(e);
        }
        return retValLa;
    }

    private int chkContract() {
        int count = 0;
        contract = false;
        CONTCON = new ArrayList();
        model.getUiDataModel().setContractlist(new ArrayList());
        model.getUiDataModel().setSelectedGridValue(new ArrayList());
        try {

            FDRforcedDischargeVO fdr;
            FDRforcedDischargeVO vo;
            List al = new ArrayList();
            for (int i = 0; i < model.getUiDataModel().getfDGridList().size(); i++) {
                vo = (FDRforcedDischargeVO) model.getUiDataModel().getfDGridList().get(i);
                if (vo.getModeL().equalsIgnoreCase("F") && "W".equalsIgnoreCase(vo.getReftype()) && vo.getSelectL()) {
                    fdr = new FDRforcedDischargeVO();
                    fdr.setoPSPOLL(vo.getoPSPOLL());
                    fdr.setoPSPOLTerminalL(vo.getoPSPOLTerminalL());
                    fdr.setoPSPODL(model.getUiDataModel().getTxtNewOpspod());
                    fdr.setoPSPODTerminalL(model.getUiDataModel().getTxtNewOpspodTer());
                    fdr.setVendor(vo.getVendor());
                    fdr.setModeL(vo.getModeL());
                    fdr.setOpsDnoL(vo.getOpsDnoL());
                    fdr.setEtd(vo.getEtd());
                    fdr.setEqptype(vo.getEqptype());
                    fdr.setServiceL(vo.getServiceL());

                    al.add(fdr);
                }
            }

            if (!al.isEmpty() && "true".equalsIgnoreCase(CON)) {
                List alr = remote.checkContract(al, secondparam);
                log.info("Contract >> " + alr);
                if (!alr.isEmpty()) {

                    FDRforcedDischargeVO vos1;
                    FDRforcedDischargeVO vo1;

                    for (int j = 0; j < model.getUiDataModel().getfDGridList().size(); j++) {
                        vos1 = (FDRforcedDischargeVO) model.getUiDataModel().getfDGridList().get(j);
                        if (alr.contains(vos1.getOpsDnoL())) {
                            vos1.setSelectL(false);
                            contract = true;

//                            CONTCON.append(vos1.getBookNoL() + " ~ " + vos1.getContainerNoL() + "\n");
                            //            log.info("BKG CNT NO >> " + vos1.getBookNoL() + " ~ " + vos1.getContainerNoL());
                            vo1 = new FDRforcedDischargeVO();
                            vo1.setBookNoL(vos1.getBookNoL().trim());
                            vo1.setContainerNoL(vos1.getContainerNoL().trim());
                            model.getUiDataModel().getContractlist().add(vo1);

                        }
                    }
                    RequestContext.getCurrentInstance().update(TABLE1);

                }
            }

            FDRforcedDischargeVO vos2;
            for (int j = 0; j < model.getUiDataModel().getfDGridList().size(); j++) {
                vos2 = (FDRforcedDischargeVO) model.getUiDataModel().getfDGridList().get(j);
                if (vos2.getSelectL().toString().equalsIgnoreCase("true")) {
                    model.getUiDataModel().getSelectedGridValue().add(vos2);
                    count++;
                }
            }
            //            log.info("count >> " + count + " Size >> " + model.getUiDataModel().getfDGridList().size());
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
        return count;
    }

    public void btnExitActionPerformed() {
        try {

            RequestContext.getCurrentInstance().execute("PF('FDR_CV').hide();");
            model.getUiDataModel().setContractlist(new ArrayList());
            model.getStateModel().setRemRow(false);
//            if (remMsg) {
//                dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020041", "", new String[]{finalmsgs});
//            }
            clearbottomfields();
            RequestContext.getCurrentInstance().update("FDR_Cont");
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void btnExitImpActionPerformed() {
        try {
            model.getStateModel().setRemImpRow(false);
            RequestContext.getCurrentInstance().execute("PF('FDR_IMP').hide();");
            RequestContext.getCurrentInstance().update("FDR_imps");
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void notplannedclose() {
        try {

            RequestContext.getCurrentInstance().execute("PF('FDR_CV').hide();");
            model.getUiDataModel().setContractlist(new ArrayList());

            model.getStateModel().setRemRow(false);
//            if (remMsg) {
//                dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020041", "", new String[]{finalmsgs});
//            }
            clearbottomfields();
            RequestContext.getCurrentInstance().update("FDR_Cont");
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void setData() {
        if (model.getStateModel().isChkmatchun()) {
        try {
            remMsg = false;
            SearchUtils su = new SearchUtils();
            su.setParameterValues(0, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewOpspod().trim());
            String agcode = su.checkCode("FDR", "6020028");
            log.info("agcode >> " + agcode);
            int count = chkContract();
            if (count > 0) {
                if (model.getStateModel().isSetSvcDet()) {
                    if (model.getUiDataModel().getTxtNewSvc().trim().equalsIgnoreCase(model.getUiDataModel().getTxtService().trim())
                            && model.getUiDataModel().getTxtNewVslCode().trim().equalsIgnoreCase(model.getUiDataModel().getTxtVslCode().trim())
                            && model.getUiDataModel().getTxtNewVoy().trim().equalsIgnoreCase(model.getUiDataModel().getTxtVoyageCode().trim())
                            && model.getUiDataModel().getTxtNewBnd().trim().equalsIgnoreCase(model.getUiDataModel().getTxtBound().trim())) {

                        model.getStateModel().setSetSvcDet(true);
                        RequestContext.getCurrentInstance().update("FDR-FDR_setSvcDet");
                    } else {
                        model.getStateModel().setSetSvcDet(false);
                    }
                }
                String opsNoForm = getOpsNoForm();
                Map hm = new HashMap();
                String cargotype = "";
                if (model.getStateModel().isAsLocal()) {
                    cargotype = "Local";
                }
                //            log.info("cargotype >> " + cargotype);
                hm.put("ser", model.getUiDataModel().getTxtService().trim());
                hm.put("vsl", model.getUiDataModel().getTxtVslCode().trim());
                hm.put("voy", model.getUiDataModel().getTxtVoyageCode().trim());
                hm.put("bnd", model.getUiDataModel().getTxtBound().trim());
                hm.put("port", model.getUiDataModel().getTxtOpspod().trim());
                hm.put("tml", model.getUiDataModel().getTxtterminal().trim());
//                hm.put("plan", model.getUiDataModel().getCmbPlan() != null ? model.getUiDataModel().getCmbPlan().trim() : "");
                hm.put("plan", cargotype);
                hm.put("opsno", opsNoForm);
                hm.put("un", mData.getUserCode() != null ? mData.getUserCode() : "");
                hm.put("newser", model.getUiDataModel().getTxtNewSvc().trim());
                hm.put("newvsl", model.getUiDataModel().getTxtNewVslCode().trim());
                hm.put("newvoy", model.getUiDataModel().getTxtNewVoy().trim());
                hm.put("newbnd", model.getUiDataModel().getTxtNewBnd().trim());
                hm.put("newport", model.getUiDataModel().getTxtNewOpspod().trim());
                hm.put("newtml", model.getUiDataModel().getTxtNewOpspodTer().trim());
                hm.put("newcall", model.getUiDataModel().getTxtNewCallId().trim());
                hm.put("oldcall", model.getUiDataModel().getTxtCallId().trim());
                hm.put("ag", agcode);
                hm.put("secondparam", secondparam);
                 String option="up";
                String retVal = (String) remote.newSetForcedDischargeun(hm,option);
                model.getUiDataModel().setContractlist(new ArrayList());
                retVal = (retVal != null) ? retVal.trim() : "";
                log.info("Operational Alignments Status ==============> " + retVal);
                log.info("secondparam ==============> " + secondparam);
                log.info("contract ==============> " + contract);
                log.info("CONTCON ==============> " + CONTCON);
                log.info("finalmsg ==============> " + finalmsg);
                if ("Success".equalsIgnoreCase(retVal)) {
                    if (finalmsg) {
                        if (contract) {
//                            String str = CONTCON.toString();
//                            dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020045", "", new String[]{str});

                            if (!model.getUiDataModel().getContractlist().isEmpty()) {
                                model.getStateModel().setShowcontpanel(true);
                                model.getStateModel().setShowimppanel(true);
                                model.getStateModel().setRemRow(true);
                                RequestContext.getCurrentInstance().execute("PF('FDR_CV').show();");
                                RequestContext.getCurrentInstance().update("FDR_Cont");
                            }

                            remMsg = true;

                        } else {
                            dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020041", "", new String[]{finalmsgs});
                        }
                    } else {

                        if (contract) {
//                            String str = CONTCON.toString();
//                            dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020044", "", new String[]{str});
                            if (!model.getUiDataModel().getContractlist().isEmpty()) {
                                model.getStateModel().setRemRow(true);
                                model.getStateModel().setShowcontpanel(true);
                                model.getStateModel().setShowimppanel(false);
                                RequestContext.getCurrentInstance().execute("PF('FDR_CV').show();");
                                RequestContext.getCurrentInstance().update("FDR_Cont");
                            }
                            remMsg = false;
                        } else {
                            dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020028", "", null);
                        }

                    }
                    model.getStateModel().setSelectenable(false);
                    model.getUiDataModel().setFilterlist(null);

                    selectedRowRemove();
                    clearbottomfields();
                    refreshResultTable(TABLE1);
                    refreshResultTable(TABLE2);
                } else {

                    dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020029", "", null);
                }

                if (model.getUiDataModel().getfDGridList().isEmpty()) {
                    model.getStateModel().setSelectenableDis(true);
                }
                RequestContext.getCurrentInstance().update(TABLE1);
                RequestContext.getCurrentInstance().update(TABLE2);
            } else {
//                dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020043", "", null);
                if (!model.getUiDataModel().getContractlist().isEmpty() && !model.getUiDataModel().getImportlist().isEmpty()) {
                    model.getStateModel().setRemRow(false);
                    model.getStateModel().setShowimppanel(true);
                    model.getStateModel().setShowcontpanel(true);
                    RequestContext.getCurrentInstance().execute("PF('FDR_CV').show();");
                    RequestContext.getCurrentInstance().update("FDR_Cont");
                } else if (!model.getUiDataModel().getContractlist().isEmpty()) {
                    model.getStateModel().setShowimppanel(false);
                    model.getStateModel().setShowcontpanel(true);
                    RequestContext.getCurrentInstance().execute("PF('FDR_CV').show();");
                    RequestContext.getCurrentInstance().update("FDR_Cont");
                }
                remMsg = false;
                model.getStateModel().setSelectenable(false);
            }

        } 
      catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }}
        else{
             try {
            remMsg = false;
            SearchUtils su = new SearchUtils();
            su.setParameterValues(0, SearchUtils.DATATYPE_STRING, model.getUiDataModel().getTxtNewOpspod().trim());
            String agcode = su.checkCode("FDR", "6020028");
            log.info("agcode >> " + agcode);
            int count = chkContract();
            if (count > 0) {
                if (model.getStateModel().isSetSvcDet()) {
                    if (model.getUiDataModel().getTxtNewSvc().trim().equalsIgnoreCase(model.getUiDataModel().getTxtService().trim())
                            && model.getUiDataModel().getTxtNewVslCode().trim().equalsIgnoreCase(model.getUiDataModel().getTxtVslCode().trim())
                            && model.getUiDataModel().getTxtNewVoy().trim().equalsIgnoreCase(model.getUiDataModel().getTxtVoyageCode().trim())
                            && model.getUiDataModel().getTxtNewBnd().trim().equalsIgnoreCase(model.getUiDataModel().getTxtBound().trim())) {

                        model.getStateModel().setSetSvcDet(true);
                        RequestContext.getCurrentInstance().update("FDR-FDR_setSvcDet");
                    } else {
                        model.getStateModel().setSetSvcDet(false);
                    }
                }
                String opsNoForm = getOpsNoForm();
                Map hm = new HashMap();
                String cargotype = "";
                if (model.getStateModel().isAsLocal()) {
                    cargotype = "Local";
                }
                //            log.info("cargotype >> " + cargotype);
                hm.put("ser", model.getUiDataModel().getTxtService().trim());
                hm.put("vsl", model.getUiDataModel().getTxtVslCode().trim());
                hm.put("voy", model.getUiDataModel().getTxtVoyageCode().trim());
                hm.put("bnd", model.getUiDataModel().getTxtBound().trim());
                hm.put("port", model.getUiDataModel().getTxtOpspod().trim());
                hm.put("tml", model.getUiDataModel().getTxtterminal().trim());
//                hm.put("plan", model.getUiDataModel().getCmbPlan() != null ? model.getUiDataModel().getCmbPlan().trim() : "");
                hm.put("plan", cargotype);
                hm.put("opsno", opsNoForm);
                hm.put("un", mData.getUserCode() != null ? mData.getUserCode() : "");
                hm.put("newser", model.getUiDataModel().getTxtNewSvc().trim());
                hm.put("newvsl", model.getUiDataModel().getTxtNewVslCode().trim());
                hm.put("newvoy", model.getUiDataModel().getTxtNewVoy().trim());
                hm.put("newbnd", model.getUiDataModel().getTxtNewBnd().trim());
                hm.put("newport", model.getUiDataModel().getTxtNewOpspod().trim());
                hm.put("newtml", model.getUiDataModel().getTxtNewOpspodTer().trim());
                hm.put("newcall", model.getUiDataModel().getTxtNewCallId().trim());
                hm.put("oldcall", model.getUiDataModel().getTxtCallId().trim());
                hm.put("ag", agcode);
                hm.put("secondparam", secondparam);
                String option="lp";
                String retVal = (String) remote.newSetForcedDischarge(hm,option);
                model.getUiDataModel().setContractlist(new ArrayList());
                retVal = (retVal != null) ? retVal.trim() : "";
                log.info("Operational Alignments Status ==============> " + retVal);
                log.info("secondparam ==============> " + secondparam);
                log.info("contract ==============> " + contract);
                log.info("CONTCON ==============> " + CONTCON);
                log.info("finalmsg ==============> " + finalmsg);
                if ("Success".equalsIgnoreCase(retVal)) {
                    if (finalmsg) {
                        if (contract) {
//                            String str = CONTCON.toString();
//                            dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020045", "", new String[]{str});

                            if (!model.getUiDataModel().getContractlist().isEmpty()) {
                                model.getStateModel().setShowcontpanel(true);
                                model.getStateModel().setShowimppanel(true);
                                model.getStateModel().setRemRow(true);
                                RequestContext.getCurrentInstance().execute("PF('FDR_CV').show();");
                                RequestContext.getCurrentInstance().update("FDR_Cont");
                            }

                            remMsg = true;

                        } else {
                            dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020041", "", new String[]{finalmsgs});
                        }
                    } else {

                        if (contract) {
//                            String str = CONTCON.toString();
//                            dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020044", "", new String[]{str});
                            if (!model.getUiDataModel().getContractlist().isEmpty()) {
                                model.getStateModel().setRemRow(true);
                                model.getStateModel().setShowcontpanel(true);
                                model.getStateModel().setShowimppanel(false);
                                RequestContext.getCurrentInstance().execute("PF('FDR_CV').show();");
                                RequestContext.getCurrentInstance().update("FDR_Cont");
                            }
                            remMsg = false;
                        } else {
                            dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020028", "", null);
                        }

                    }
                    model.getStateModel().setSelectenable(false);
                    model.getUiDataModel().setFilterlist(null);

                    selectedRowRemove();
                    clearbottomfields();
                    refreshResultTable(TABLE1);
                    refreshResultTable(TABLE2);
                } else {

                    dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020029", "", null);
                }

                if (model.getUiDataModel().getfDGridList().isEmpty()) {
                    model.getStateModel().setSelectenableDis(true);
                }
                RequestContext.getCurrentInstance().update(TABLE1);
                RequestContext.getCurrentInstance().update(TABLE2);
            } else {
//                dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020043", "", null);
                if (!model.getUiDataModel().getContractlist().isEmpty() && !model.getUiDataModel().getImportlist().isEmpty()) {
                    model.getStateModel().setRemRow(false);
                    model.getStateModel().setShowimppanel(true);
                    model.getStateModel().setShowcontpanel(true);
                    RequestContext.getCurrentInstance().execute("PF('FDR_CV').show();");
                    RequestContext.getCurrentInstance().update("FDR_Cont");
                } else if (!model.getUiDataModel().getContractlist().isEmpty()) {
                    model.getStateModel().setShowimppanel(false);
                    model.getStateModel().setShowcontpanel(true);
                    RequestContext.getCurrentInstance().execute("PF('FDR_CV').show();");
                    RequestContext.getCurrentInstance().update("FDR_Cont");
                }
                remMsg = false;
                model.getStateModel().setSelectenable(false);
            }

        } 
      catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        
        }
    }}

    public void selectedRowRemove() {
        try {

            int planModel = model.getUiDataModel().getfDGridList().size();
            FDRforcedDischargeVO avo;
            for (int l = planModel - 1; l >= 0; l--) {

                avo = (FDRforcedDischargeVO) model.getUiDataModel().getfDGridList().get(l);
                if (avo.getSelectL()) {
                    model.getUiDataModel().getfDGridList().remove(avo);
                }
            }

            if (!model.getUiDataModel().getfDGridList().isEmpty()) {
                model.getStateModel().setButtonGroup2Dis(false);

            } else {
                model.getUiDataModel().setSelectedGridValue1(new ArrayList());
                model.getUiDataModel().setMatchingGridList(new ArrayList());
                model.getStateModel().setButtonGroup2Dis(true);
            }
            RequestContext.getCurrentInstance().update(TABLE1);
            RequestContext.getCurrentInstance().update(TABLE2);

        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public String getOpsNoForm() {
        String opsNos = "";
        try {
            String tmp;

            List planModel = (ArrayList) model.getUiDataModel().getfDGridList();
            FDRforcedDischargeVO vo;
            for (Object planModel1 : planModel) {
                vo = (FDRforcedDischargeVO) planModel1;
                if ("true".equalsIgnoreCase(vo.getSelectL().toString())) {
                    tmp = vo.getOpsDnoL();
                    tmp = (tmp != null) ? tmp.trim() : "";
                    opsNos = opsNos + "," + tmp;
                }
            }
            opsNos = (opsNos.length() > 0) ? opsNos.substring(1) : "";
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
        return opsNos;
    }

    public void disableTheComp() {
        try {

            model.getStateModel().setjBtnSerDis(true);
            model.getStateModel().setjBtnVslDis(true);
            model.getStateModel().setjBtnVygDis(true);
            model.getStateModel().setBtnPortDis(true);
            model.getStateModel().setBtnterminalDis(true);
            model.getStateModel().setBtnShowDis(true);
            model.getStateModel().setBtnConfDis(true);
            model.getStateModel().setCheck1Dis(true);

            model.getStateModel().setButtonGroup2Dis(true);
            model.getStateModel().setBtnErrorDis(true);
            model.getStateModel().setBtnPortNewDis(true);
            model.getStateModel().setBtnterminalNewDis(true);

        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void enableTheComp() {
        try {
            model.getStateModel().setAsLocal(false);
            model.getStateModel().setjBtnSerDis(false);
            model.getStateModel().setjBtnVslDis(false);
            model.getStateModel().setjBtnVygDis(false);
            model.getStateModel().setBtnPortDis(false);
            model.getStateModel().setSelectenable(false);
            if (model.getUiDataModel().getTxtOpspod() != null && model.getUiDataModel().getTxtOpspod().startsWith("ZF")) {
                model.getStateModel().setBtnterminalDis(true);
            } else {
                model.getStateModel().setBtnterminalDis(false);
            }
            model.getStateModel().setBtnShowDis(false);
            model.getStateModel().setBtnConfDis(false);
            model.getStateModel().setCheck1Dis(false);

            model.getStateModel().setBtnErrorDis(false);
            model.getStateModel().setBtnPortNewDis(false);
            model.getStateModel().setBtnterminalNewDis(false);

            if (!model.getUiDataModel().getfDGridList().isEmpty()) {
                model.getStateModel().setBtnFilter(false);
                model.getStateModel().setJbtnSearchDis(false);
                model.getStateModel().setBtnErrorDis(false);
                model.getStateModel().setBtnConfDis(false);
                model.getStateModel().setCheck1Dis(false);
                model.getStateModel().setBtnPortNewDis(false);
                model.getStateModel().setBtnterminalNewDis(false);

                model.getStateModel().setButtonGroup2Dis(false);

                model.getUiDataModel().setMatchingGridList(new ArrayList());
                model.getStateModel().setSetSvcDetDis(false);
                model.getStateModel().setSvcRefreshDis(false);
                model.getStateModel().setTxtNewSvcDis(false);
                model.getStateModel().setBtnNewSvcDis(false);
                model.getStateModel().setNewSerRO(false);
                model.getStateModel().setTxtNewVslCodeDis(false);
                model.getStateModel().setBtnNewVslDis(false);
                model.getStateModel().setTxtNewVslNameDis(false);
                model.getStateModel().setTxtNewVoyDis(false);
                model.getStateModel().setBtnNewVoyBndDis(false);
                model.getStateModel().setTxtNewBndDis(false);
                model.getStateModel().setTxtNewOpspodDis(false);
                model.getStateModel().setBtnNewOpspodDis(false);
                model.getStateModel().setBtnNewOpspodTerDis(false);
                model.getStateModel().setTxtNewOpspodTerDis(false);
                setDefaultEmptyRow();
                RequestContext.getCurrentInstance().update("FDR-FDR_buttonGroup2");
            } else {
                model.getStateModel().setBtnFilter(true);
                dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020037", "", null);
                model.getStateModel().setJbtnSearchDis(true);
                model.getStateModel().setBtnErrorDis(true);

                model.getStateModel().setCheckDis(true);
                model.getStateModel().setBtnPortNewDis(true);
                model.getStateModel().setBtnterminalNewDis(true);
                model.getStateModel().setCheck1Dis(true);

                model.getStateModel().setButtonGroup2Dis(true);

                clearbottomfields();
            }
            if (BKGNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {

                model.getUiDataModel().setCmbPlan("");
                model.getStateModel().setCmbPlanDis(false);
            } else {

                model.getUiDataModel().setCmbPlan(LOCAL);
                model.getStateModel().setCmbPlanDis(true);
            }

        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void enableComponent() {
        try {

            model.getStateModel().setjBtnSerDis(false);
            model.getStateModel().setjBtnVslDis(false);
            model.getStateModel().setjBtnVygDis(false);
            model.getStateModel().setBtnPortDis(false);
            model.getStateModel().setTxtVslCodeDis(false);
            model.getStateModel().setTxtVoyageCodeDis(false);
            if (model.getUiDataModel().getTxtOpspod() != null && model.getUiDataModel().getTxtOpspod().startsWith("ZF")) {

                model.getStateModel().setBtnterminalDis(true);
            } else {
                model.getStateModel().setBtnterminalDis(false);
            }
            model.getStateModel().setBtnShowDis(false);
            model.getStateModel().setCheck1Dis(false);
            model.getStateModel().setBtnErrorDis(false);

            model.getStateModel().setBtnConfDis(false);
            model.getStateModel().setCmbPlanDis(true);
            model.getStateModel().setBtnShowDis(false);

            model.getStateModel().setButtonGroup2Dis(true);

            model.getStateModel().setTxtServiceDis(false);
            model.getStateModel().setTxtVslCodeDis(false);
            model.getStateModel().setTxtVslNameDis(false);

            model.getStateModel().setTxtVoyageCodeDis(false);
            model.getStateModel().setTxtBoundDis(false);
            model.getStateModel().setTxtOpspodDis(false);
            model.getStateModel().setTxtterminalDis(false);

        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void voyclearfield() {
        try {
            model.getUiDataModel().setTxtBound("");
            model.getUiDataModel().setTxtOpspod("");
            model.getUiDataModel().setTxtterminal("");

            model.getUiDataModel().setfDGridList(new ArrayList());
            model.getUiDataModel().setMatchingGridList(new ArrayList());
            refreshResultTable(TABLE1);
            refreshResultTable(TABLE2);
            clearbottomfields();

        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    private void newActionMethod() {
        try {
            enableComponent();
            model.getStateModel().setGridhdr1(true);
            model.getStateModel().setGridhdr2(false);
            model.getUiDataModel().setTxtCallOrder("0");
            model.getUiDataModel().setTxtService("");
            model.getUiDataModel().setTxtVslCode("");
            model.getUiDataModel().setTxtVoyageCode("");
            model.getUiDataModel().setTxtBound("");
            model.getUiDataModel().setTxtOpspod("");
            model.getUiDataModel().setTxtterminal("");
            model.getUiDataModel().setTxtSearch("");
            model.getUiDataModel().setsETADate("");
            model.getUiDataModel().setsETDDate("");
            model.getUiDataModel().setTxtVslName("");
            model.getUiDataModel().setTxtOpspodNew("");
            model.getUiDataModel().setTxtterminalNew("");
            model.getUiDataModel().setTxtDisCallId("");

            model.getStateModel().setCheckDis(true);
            model.getStateModel().setBtnConfDis(false);
            model.getStateModel().setJbtnSearchDis(true);

            model.getUiDataModel().setButtonGroup2(BKGNO);
            model.getStateModel().setBtnErrorDis(true);
            model.getStateModel().setCheck1Dis(true);
            model.getStateModel().setAsLocal(false);
            model.getStateModel().setSetSvcDetDis(true);
            model.getStateModel().setSvcRefreshDis(true);
            clearbottomfields();
            model.getUiDataModel().setfDGridList(new ArrayList());
            model.getUiDataModel().setMatchingGridList(new ArrayList());
            model.getStateModel().setTxtVslCodeDis(false);
            model.getStateModel().setTxtVoyageCodeDis(false);
            model.getStateModel().setSelectenable(false);
            model.getStateModel().setSelectenableDis(true);
            model.getStateModel().setBtnConfDis(true);
            model.getUiDataModel().setFilterlist(null);
          model.getStateModel().setChkmatchundis(false);
            model.getStateModel().setChkmatchdis(false);
            model.getStateModel().setBtnFilter(true);
//            model.getUiDataModel().setGridStyle("sm-margin-right2 sm-table-wrap sm-border");
//            model.getUiDataModel().setTable1size("ui-g-12 ui-lg-9 ui-md-12 sm-padding0 sm-margin-top0 sm-input-size84");
//            model.getUiDataModel().setTable2size("ui-g-12 ui-lg-3 ui-md-12 sm-padding0 sm-margin-top0 sm-input-size16");
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void newactionperformed(ActionEvent event) {
        try {
            if (this.bean.isModuleLaunching()) {
                newActionMethod();
            } else {
                dutil.showDialog(FDR, SMDialogUtil.type.CONF, "06020026", "#{fdrCBean.ctrl.btnNew}", null);
            }
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void btnCancelActionPerformed() {
        try {
            OPTIONBUTTON = "";

            model.getUiDataModel().setTxtService("");
            model.getUiDataModel().setTxtVslCode("");
            model.getUiDataModel().setTxtVoyageCode("");
            model.getUiDataModel().setTxtBound("");
            model.getUiDataModel().setTxtOpspod("");
            model.getUiDataModel().setTxtterminal("");
            model.getUiDataModel().setTxtSearch("");
            model.getUiDataModel().setsETADate("");
            model.getUiDataModel().setsETDDate("");
            model.getUiDataModel().setTxtVslName("");
            model.getUiDataModel().setTxtOpspodNew("");
            model.getUiDataModel().setTxtterminalNew("");
            model.getUiDataModel().setTxtDisCallId("");
            disableComponent();
            model.getUiDataModel().setfDGridList(new ArrayList());
            model.getUiDataModel().setMatchingGridList(new ArrayList());

            model.getUiDataModel().setBtnSelectValue(SELECTALL);
            model.getUiDataModel().setCmbPlanlist(new ArrayList());
            model.getUiDataModel().setButtonGroup2(BKGNO);
            model.getStateModel().setGridhdr1(true);
            model.getStateModel().setGridhdr2(false);

            model.getUiDataModel().setBookcntrno("Book No / WO No");
            model.getUiDataModel().setLblStats("~ Matched Book No / WO No");
            model.getUiDataModel().setLblStats1("~ Unmatched Book No / WO No");

            clearbottomfields();
           model.getStateModel().setChkmatchundis(true);
            model.getStateModel().setChkmatchdis(true);
            if (model.getStateModel().isChkmatch()) {
                model.getUiDataModel().setGridStyle("sm-margin-right2 sm-table-wrap sm-border");
                model.getUiDataModel().setTable1size("ui-g-12 ui-lg-9 ui-md-12 sm-padding0 sm-margin-top0 sm-input-size84");
                model.getUiDataModel().setTable2size("ui-g-12 ui-lg-3 ui-md-12 sm-padding0 sm-margin-top0 sm-input-size16");

            }
            ToolBarUtils.setState(ToolbarState.CANCEL, bean);
            refreshResultTable(TABLE1);
            refreshResultTable(TABLE2);
            RequestContext.getCurrentInstance().update(FDR);

        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void btnNew() {
        try {
            OPTIONBUTTON = "";

            String eventType = (String) UIComponent.getCurrentComponent(FacesContext.getCurrentInstance()).getAttributes().get("eventType");
            if ("yes".equalsIgnoreCase(eventType)) {
                newActionMethod();
                ToolBarUtils.setState(ToolbarState.NEW, bean);
            }
            RequestContext.getCurrentInstance().update(FDR);
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    private void clearbottomfields() {
        model.getStateModel().setAsLocal(false);
        model.getStateModel().setSetSvcDet(false);
        model.getStateModel().setBtnNewSvcDis(false);
        model.getStateModel().setNewSerRO(false);
        model.getStateModel().setBtnNewVslDis(false);
        model.getStateModel().setBtnNewVoyBndDis(false);
        model.getUiDataModel().setTxtNewSvc("");
        model.getUiDataModel().setTxtNewVslCode("");
        model.getUiDataModel().setTxtNewVslName("");
        model.getUiDataModel().setTxtNewVoy("");
        model.getUiDataModel().setTxtNewBnd("");
        model.getUiDataModel().setTxtNewOpspod("");
        model.getUiDataModel().setTxtNewOpspodTer("");
        model.getUiDataModel().setTxtNewCallId("");
        model.getUiDataModel().setCmbPlan("");

        if (model.getUiDataModel().getfDGridList().isEmpty()) {
            model.getStateModel().setTxtNewCallIdDis(true);
            model.getStateModel().setTxtNewSvcDis(true);
            model.getStateModel().setBtnNewSvcDis(true);
            model.getStateModel().setNewSerRO(true);
            model.getStateModel().setTxtNewVslCodeDis(true);
            model.getStateModel().setBtnNewVslDis(true);
            model.getStateModel().setTxtNewVslNameDis(true);
            model.getStateModel().setTxtNewVoyDis(true);
            model.getStateModel().setBtnNewVoyBndDis(true);
            model.getStateModel().setTxtNewBndDis(true);
            model.getStateModel().setTxtNewOpspodDis(true);
            model.getStateModel().setBtnNewOpspodDis(true);
            model.getStateModel().setBtnNewOpspodTerDis(true);

            model.getStateModel().setSetSvcDetDis(true);
            model.getStateModel().setSvcRefreshDis(true);
            model.getStateModel().setCmbPlanDis(true);
            model.getStateModel().setTxtNewOpspodTerDis(true);
            model.getStateModel().setBtnConfDis(true);
        }

        RequestContext.getCurrentInstance().update(FRAGB);
        RequestContext.getCurrentInstance().update(FRAGB1);
        RequestContext.getCurrentInstance().update(FRAGC);

    }

    public void cmbPlanItemStateChanged(AjaxBehaviorEvent ae) {
        try {

            RequestContext.getCurrentInstance().update("FDR-FDR_local");
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void check1ActionPerformed() {
        try {

            model.getUiDataModel().setSelectedGridValue(new ArrayList());

            List gridlist = new ArrayList();

            if (model.getUiDataModel().getFilterlist() != null && !model.getUiDataModel().getFilterlist().isEmpty()) {
                gridlist = model.getUiDataModel().getFilterlist();
            } else if (model.getUiDataModel().getfDGridList() != null && !model.getUiDataModel().getfDGridList().isEmpty()) {
                gridlist = model.getUiDataModel().getfDGridList();
            }
            boolean isSelectAll = checkSelectedorNot(gridlist);
            for (Object gridlist1 : gridlist) {
                FDRforcedDischargeVO vo11 = (FDRforcedDischargeVO) gridlist1;
                if (isSelectAll) {
                    vo11.setSelectL(false);
                    model.getStateModel().setSelectenable(false);
                } else {
                    vo11.setSelectL(true);
                    model.getStateModel().setSelectenable(true);

                }
            }

            for (int j = 0; j < model.getUiDataModel().getfDGridList().size(); j++) {
                FDRforcedDischargeVO vo1 = (FDRforcedDischargeVO) model.getUiDataModel().getfDGridList().get(j);
                if (vo1.getSelectL()) {

                    model.getUiDataModel().getSelectedGridValue().add(vo1);
                }
            }
            checkuncheck();
            log.info("chkaction inside>>>>>"+model.getUiDataModel().getfDGridList().size());
            RequestContext.getCurrentInstance().update(TABLE1);

        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    private boolean checkSelectedorNot(List gridlist) {
        boolean isSelectAll = true;
        try {
            if ((gridlist != null) && (!gridlist.isEmpty())) {
                for (Object gridlist1 : gridlist) {
                    FDRforcedDischargeVO vo11 = (FDRforcedDischargeVO) gridlist1;
                    if (!vo11.getSelectL()) {
                        isSelectAll = false;
                        break;
                    }
                }
            }
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
        return isSelectAll;
    }

    public void getSearchData(String string, Object o) {
        try {
            log.info(" >> " + string + " >> " + o);
            switch (string) {
                case "060200011": 
                case "06020001": 
                    servieSearch((ArrayList) o);
                    break;
                    
                 case "060200022": 
                case "06020002": 
                    vesselSearch((ArrayList) o);
                    break;
                    
                case "060200033": 
                case "06020003": 
                    voyageSearch((ArrayList) o);
                    break;
                    
                case "060200044": 
                case "06020004": 
                    portSearch((ArrayList) o);
                    break;
                    
                   case "060200055": 
                case "06020005": 
                    terminalSearch((ArrayList) o);
                    break;
                
                case "06020006":
                case "6020022":
                    newSvcSearch((ArrayList) o);
                    break;

                case "06020007":
                case "6020023":

                    newSvcSearch1((ArrayList) o);
                    break;

                case "06020008":
                case "6020024":
                    newVslSearch((ArrayList) o);
                    break;

                case "06020009":
                case "6020025":
                    newVoySearch((ArrayList) o);
                    break;

                case "06020010":
                case "6020026":
                    newOpspodSearch((ArrayList) o);
                    break;

                case "06020011":
                case "6020027":
                    newOpspodTerSearch((ArrayList) o);
                    break;

                case "6020013": {
                    easySearch((ArrayList) o);
                    break;
                }
                case "6020014":
                case "6020020":
                    newSerDetSet((ArrayList) o);
                    break;
            }
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void newSerDetSet(List s) {
        try {
            if (s == null) {
                model.getUiDataModel().setTxtNewSvc("");
                model.getUiDataModel().setTxtNewVslCode("");
                model.getUiDataModel().setTxtNewVslName("");
                model.getUiDataModel().setTxtNewVoy("");
                model.getUiDataModel().setTxtNewBnd("");
                model.getUiDataModel().setTxtNewOpspod("");
                model.getUiDataModel().setTxtNewOpspodTer("");
                model.getUiDataModel().setTxtNewCallId("");
            } else {
                model.getUiDataModel().setTxtNewSvc(nullChecker(s.get(0)));
                model.getUiDataModel().setTxtNewVslCode(nullChecker(s.get(2)));
                model.getUiDataModel().setTxtNewVslName(nullChecker(s.get(3)));
                model.getUiDataModel().setTxtNewVoy(nullChecker(s.get(4)));
                model.getUiDataModel().setTxtNewBnd(nullChecker(s.get(5)));
                model.getUiDataModel().setTxtNewOpspod(nullChecker(s.get(6)));
                model.getUiDataModel().setTxtNewOpspodTer(nullChecker(s.get(7)));
                model.getUiDataModel().setTxtNewCallId(nullChecker(s.get(8)));

            }
            RequestContext.getCurrentInstance().update(FRAGB);
            RequestContext.getCurrentInstance().update(FRAGB1);

        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }

    }

    public void easySearch(List s) {
        try {
            if (s == null) {
                model.getUiDataModel().setTxtService("");
                model.getUiDataModel().setTxtVslCode("");
                model.getUiDataModel().setTxtVslName("");
                model.getUiDataModel().setTxtVoyageCode("");
                model.getUiDataModel().setTxtBound("");
                model.getUiDataModel().setTxtOpspod("");
                model.getUiDataModel().setTxtterminal("");
                model.getUiDataModel().setTxtCallId("");
            } else {
                model.getUiDataModel().setTxtService(nullChecker(s.get(0)));
                model.getUiDataModel().setTxtVslCode(nullChecker(s.get(2)));
                model.getUiDataModel().setTxtVslName(nullChecker(s.get(3)));
                model.getUiDataModel().setTxtVoyageCode(nullChecker(s.get(4)));
                model.getUiDataModel().setTxtBound(nullChecker(s.get(5)));
                model.getUiDataModel().setTxtOpspod(nullChecker(s.get(6)));
                model.getUiDataModel().setTxtterminal(nullChecker(s.get(7)));
                model.getUiDataModel().setTxtCallId(nullChecker(s.get(8)));
            }
            OPTIONBUTTON = "";
            model.getStateModel().setBtnFilter(true);
            model.getStateModel().setCheck1Dis(true);
            model.getStateModel().setBtnPortNewDis(true);
            model.getStateModel().setBtnterminalNewDis(true);

            model.getStateModel().setButtonGroup2Dis(true);

            model.getStateModel().setCheckDis(true);
            model.getUiDataModel().setTxtSearch("");

            model.getStateModel().setJbtnSearchDis(true);
            model.getStateModel().setBtnErrorDis(true);

//            model.getUiDataModel().setButtonGroup2(BKGNO);
            if (!model.getUiDataModel().getfDGridList().isEmpty()) {
                model.getUiDataModel().setfDGridList(new ArrayList());
                model.getUiDataModel().setMatchingGridList(new ArrayList());
                model.getStateModel().setSelectenable(false);
                model.getStateModel().setSelectenableDis(true);
                model.getUiDataModel().setSelectedGridValue(new ArrayList());
                clearbottomfields();
                refreshResultTable(TABLE1);
                refreshResultTable(TABLE2);
                RequestContext.getCurrentInstance().update("FDR-FDR_FRAGBOTHTABLE");
            }
            RequestContext.getCurrentInstance().update(FRAG);
            RequestContext.getCurrentInstance().update(FRAG1);

        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }

    }

    public void serchange() {
        try {
            model.getUiDataModel().setTxtVslCode("");
            model.getUiDataModel().setTxtVslName("");
            model.getUiDataModel().setTxtVoyageCode("");
            model.getUiDataModel().setTxtBound("");
            model.getUiDataModel().setTxtOpspod("");
            model.getUiDataModel().setTxtterminal("");
            model.getUiDataModel().setTxtOpspodNew("");
            model.getUiDataModel().setTxtterminalNew("");
            model.getUiDataModel().setTxtCallOrder("");
            model.getUiDataModel().setTxtDisCallId("");
            model.getUiDataModel().setsETADate("");
            model.getUiDataModel().setsETDDate("");
            model.getUiDataModel().setTxtSearch("");

        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void veschange() {
        try {
            model.getUiDataModel().setTxtVoyageCode("");
            model.getUiDataModel().setTxtBound("");
            model.getUiDataModel().setTxtOpspod("");
            model.getUiDataModel().setTxtterminal("");
            model.getUiDataModel().setTxtOpspodNew("");
            model.getUiDataModel().setTxtterminalNew("");
            model.getUiDataModel().setTxtCallOrder("");
            model.getUiDataModel().setTxtDisCallId("");
            model.getUiDataModel().setsETADate("");
            model.getUiDataModel().setsETDDate("");
            model.getUiDataModel().setTxtSearch("");

        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void voychange() {
        try {
            model.getUiDataModel().setTxtOpspod("");
            model.getUiDataModel().setTxtterminal("");
            model.getUiDataModel().setTxtOpspodNew("");
            model.getUiDataModel().setTxtterminalNew("");
            model.getUiDataModel().setTxtCallOrder("");
            model.getUiDataModel().setTxtDisCallId("");
            model.getUiDataModel().setsETADate("");
            model.getUiDataModel().setsETDDate("");
            model.getUiDataModel().setTxtSearch("");

        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void portchange() {
        try {
            model.getUiDataModel().setTxtterminal("");
            model.getUiDataModel().setTxtOpspodNew("");
            model.getUiDataModel().setTxtterminalNew("");
            model.getUiDataModel().setTxtCallOrder("");
            model.getUiDataModel().setTxtDisCallId("");
            model.getUiDataModel().setsETADate("");
            model.getUiDataModel().setsETDDate("");
            model.getUiDataModel().setTxtSearch("");

        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void termchange() {
        try {
            model.getUiDataModel().setTxtOpspodNew("");
            model.getUiDataModel().setTxtterminalNew("");
            model.getUiDataModel().setTxtCallOrder("");
            model.getUiDataModel().setTxtDisCallId("");
            model.getUiDataModel().setsETADate("");
            model.getUiDataModel().setsETDDate("");
            model.getUiDataModel().setTxtSearch("");

        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void newserchange() {
        try {
            model.getUiDataModel().setTxtNewVslCode("");
            model.getUiDataModel().setTxtNewVslName("");
            model.getUiDataModel().setTxtNewVoy("");
            model.getUiDataModel().setTxtNewBnd("");
            model.getUiDataModel().setTxtNewOpspod("");
            model.getUiDataModel().setTxtNewOpspodTer("");
            model.getUiDataModel().setTxtNewCallId("");

        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void newvslchange() {
        try {
            model.getUiDataModel().setTxtNewVslCode("");
            model.getUiDataModel().setTxtNewVslName("");
            model.getUiDataModel().setTxtNewVoy("");
            model.getUiDataModel().setTxtNewBnd("");
            model.getUiDataModel().setTxtNewOpspod("");
            model.getUiDataModel().setTxtNewOpspodTer("");
            model.getUiDataModel().setTxtNewCallId("");

        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void newvoychange() {
        try {
            model.getUiDataModel().setTxtNewVoy("");
            model.getUiDataModel().setTxtNewBnd("");
            model.getUiDataModel().setTxtNewOpspod("");
            model.getUiDataModel().setTxtNewOpspodTer("");
            model.getUiDataModel().setTxtNewCallId("");
            RequestContext.getCurrentInstance().update(FRAGB);
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void newportchange() {
        try {
            model.getUiDataModel().setTxtNewOpspod("");
            model.getUiDataModel().setTxtNewOpspodTer("");
            model.getUiDataModel().setTxtNewCallId("");
            RequestContext.getCurrentInstance().update(FRAGB);
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void servieSearch(List s) {
        String service = nullChecker(s.get(0));
        serchange();
        model.getUiDataModel().setTxtService(service);

        model.getStateModel().setCheck1Dis(true);
        model.getStateModel().setBtnPortNewDis(true);
        model.getStateModel().setBtnterminalNewDis(true);

        model.getStateModel().setButtonGroup2Dis(true);

        model.getStateModel().setCheckDis(true);
        model.getUiDataModel().setTxtSearch("");

        model.getStateModel().setJbtnSearchDis(true);
        model.getStateModel().setBtnErrorDis(true);

//        model.getUiDataModel().setButtonGroup2(BKGNO);
        OPTIONBUTTON = "";
        model.getStateModel().setBtnFilter(true);

        RequestContext.getCurrentInstance().update(FRAG);
        RequestContext.getCurrentInstance().update(FRAG1);

        if (!model.getUiDataModel().getfDGridList().isEmpty()) {
            model.getUiDataModel().setfDGridList(new ArrayList());
            model.getUiDataModel().setMatchingGridList(new ArrayList());
            model.getStateModel().setSelectenable(false);
            model.getStateModel().setSelectenableDis(true);
            model.getUiDataModel().setSelectedGridValue(new ArrayList());
            clearbottomfields();
            refreshResultTable(TABLE1);
            refreshResultTable(TABLE2);
            RequestContext.getCurrentInstance().update("FDR-FDR_FRAGBOTHTABLE");
        }

    }

    public void vesselSearch(List s) {
        String vesselcode = nullChecker(s.get(0));
        String vesselname = nullChecker(s.get(1));
        veschange();
        model.getUiDataModel().setTxtVslCode(vesselcode);
        model.getUiDataModel().setTxtVslName(vesselname);

        model.getStateModel().setCheck1Dis(true);
        model.getStateModel().setBtnPortNewDis(true);
        model.getStateModel().setBtnterminalNewDis(true);

        model.getStateModel().setButtonGroup2Dis(true);

        model.getStateModel().setCheckDis(true);
        model.getUiDataModel().setTxtSearch("");

        model.getStateModel().setJbtnSearchDis(true);
        model.getStateModel().setBtnErrorDis(true);

//        model.getUiDataModel().setButtonGroup2(BKGNO);
        OPTIONBUTTON = "";
        model.getStateModel().setBtnFilter(true);

        RequestContext.getCurrentInstance().update(FRAG);
        RequestContext.getCurrentInstance().update(FRAG1);
        if (!model.getUiDataModel().getfDGridList().isEmpty()) {
            model.getUiDataModel().setfDGridList(new ArrayList());
            model.getUiDataModel().setMatchingGridList(new ArrayList());
            model.getStateModel().setSelectenable(false);
            model.getStateModel().setSelectenableDis(true);
            model.getUiDataModel().setSelectedGridValue(new ArrayList());
            clearbottomfields();
            refreshResultTable(TABLE1);
            refreshResultTable(TABLE2);
            RequestContext.getCurrentInstance().update("FDR-FDR_FRAGBOTHTABLE");
        }
    }

    public void voyageSearch(List s) {
        String voyage = nullChecker(s.get(0));
        String bound = nullChecker(s.get(1));
        voychange();
        model.getUiDataModel().setTxtVoyageCode(voyage);
        model.getUiDataModel().setTxtBound(bound);

        model.getStateModel().setCheck1Dis(true);
        model.getStateModel().setBtnPortNewDis(true);
        model.getStateModel().setBtnterminalNewDis(true);

        model.getStateModel().setButtonGroup2Dis(true);

        model.getStateModel().setCheckDis(true);
        model.getUiDataModel().setTxtSearch("");

        model.getStateModel().setJbtnSearchDis(true);
        model.getStateModel().setBtnErrorDis(true);

//        model.getUiDataModel().setButtonGroup2(BKGNO);
        OPTIONBUTTON = "";
        model.getStateModel().setBtnFilter(true);

        RequestContext.getCurrentInstance().update(FRAG);
        RequestContext.getCurrentInstance().update(FRAG1);
        if (!model.getUiDataModel().getfDGridList().isEmpty()) {
            model.getUiDataModel().setfDGridList(new ArrayList());
            model.getUiDataModel().setMatchingGridList(new ArrayList());
            model.getStateModel().setSelectenable(false);
            model.getStateModel().setSelectenableDis(true);
            model.getUiDataModel().setSelectedGridValue(new ArrayList());
            clearbottomfields();
            refreshResultTable(TABLE1);
            refreshResultTable(TABLE2);
            RequestContext.getCurrentInstance().update("FDR-FDR_FRAGBOTHTABLE");
        }
    }

    public void portSearch(List s) {
        String port = nullChecker(s.get(0));
        portchange();
        model.getUiDataModel().setTxtOpspod(port);

        if (model.getUiDataModel().getTxtOpspod().startsWith("ZF")) {
            model.getStateModel().setTxtterminalDis(true);
            model.getStateModel().setBtnterminalDis(true);
        } else {
            model.getStateModel().setTxtterminalDis(false);
            model.getStateModel().setBtnterminalDis(false);
        }

        model.getStateModel().setCheck1Dis(true);
        model.getStateModel().setBtnPortNewDis(true);
        model.getStateModel().setBtnterminalNewDis(true);

        model.getStateModel().setButtonGroup2Dis(true);

        model.getStateModel().setCheckDis(true);
        model.getUiDataModel().setTxtSearch("");

        model.getStateModel().setJbtnSearchDis(true);
        model.getStateModel().setBtnErrorDis(true);

//        model.getUiDataModel().setButtonGroup2(BKGNO);
        OPTIONBUTTON = "";
        model.getStateModel().setBtnFilter(true);

        RequestContext.getCurrentInstance().update(FRAG1);
        if (!model.getUiDataModel().getfDGridList().isEmpty()) {
            model.getUiDataModel().setfDGridList(new ArrayList());
            model.getUiDataModel().setMatchingGridList(new ArrayList());
            model.getStateModel().setSelectenable(false);
            model.getStateModel().setSelectenableDis(true);
            model.getUiDataModel().setSelectedGridValue(new ArrayList());
            clearbottomfields();
            refreshResultTable(TABLE1);
            refreshResultTable(TABLE2);
            RequestContext.getCurrentInstance().update("FDR-FDR_FRAGBOTHTABLE");
        }

    }

    public void terminalSearch(List s) {
        try {

            String[] sp = nullChecker(s.get(0)).split("~");

            String terminal = sp[0];
            String callid = sp[1];
            termchange();
            model.getUiDataModel().setTxtterminal(terminal);
            model.getUiDataModel().setTxtCallId(callid);

            model.getStateModel().setCheck1Dis(true);
            model.getStateModel().setBtnPortNewDis(true);
            model.getStateModel().setBtnterminalNewDis(true);

            model.getStateModel().setButtonGroup2Dis(true);

            model.getStateModel().setCheckDis(true);
            model.getUiDataModel().setTxtSearch("");

            model.getStateModel().setJbtnSearchDis(true);
            model.getStateModel().setBtnErrorDis(true);

//            model.getUiDataModel().setButtonGroup2(BKGNO);
            OPTIONBUTTON = "";
            model.getStateModel().setBtnFilter(true);

            RequestContext.getCurrentInstance().update(FRAG1);
            if (!model.getUiDataModel().getfDGridList().isEmpty()) {
                model.getUiDataModel().setfDGridList(new ArrayList());
                model.getUiDataModel().setMatchingGridList(new ArrayList());
                model.getStateModel().setSelectenable(false);
                model.getStateModel().setSelectenableDis(true);
                model.getUiDataModel().setSelectedGridValue(new ArrayList());
                clearbottomfields();
                refreshResultTable(TABLE1);
                refreshResultTable(TABLE2);
                RequestContext.getCurrentInstance().update("FDR-FDR_FRAGBOTHTABLE");
            }
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void newSvcSearch(List s) {
        String newservice = nullChecker(s.get(0));
        newserchange();
        model.getUiDataModel().setTxtNewSvc(newservice);
        RequestContext.getCurrentInstance().update(FRAGB);

    }

    public void newSvcSearch1(List s) {
        String newservice = nullChecker(s.get(0));
        newvslchange();
        model.getUiDataModel().setTxtNewSvc(newservice);
        RequestContext.getCurrentInstance().update(FRAGB);

    }

    public void newVslSearch(List s) {
        String newvessel = nullChecker(s.get(0));
        String newvesselname = nullChecker(s.get(1));

        model.getUiDataModel().setTxtNewVslCode(newvessel);
        model.getUiDataModel().setTxtNewVslName(newvesselname);
        newvoychange();

    }

    public void newVoySearch(List s) {
        String newvoyage = nullChecker(s.get(0));
        String newbound = nullChecker(s.get(1));

        model.getUiDataModel().setTxtNewVoy(newvoyage);
        model.getUiDataModel().setTxtNewBnd(newbound);
        newportchange();

    }

    public void newOpspodSearch(List s) {
        String newport = nullChecker(s.get(0));
        model.getUiDataModel().setTxtNewOpspod(newport);
        model.getUiDataModel().setTxtNewOpspodTer("");
        model.getUiDataModel().setTxtNewCallId("");
        RequestContext.getCurrentInstance().update(FRAGB);
    }

    public void newOpspodTerSearch(List s) {
        String[] d = nullChecker(s.get(0)).split("~");
        secondparam = nullChecker(s.get(1));
        secondparam = (secondparam != null) ? secondparam.trim() : "";
        String newterminal = d[0];
        model.getUiDataModel().setTxtNewOpspodTer(newterminal);
        model.getUiDataModel().setTxtNewCallId(d[2]);
        RequestContext.getCurrentInstance().update(FRAGB);
    }

    public void loadingservicechange() {
        try {
            if (model.getStateModel().isSetSvcDet()) {
                //            log.info("Same as loading selected====>>");
                model.getUiDataModel().setTxtNewSvc(model.getUiDataModel().getTxtService().trim());
                model.getUiDataModel().setTxtNewVslCode(model.getUiDataModel().getTxtVslCode().trim());
                model.getUiDataModel().setTxtNewVslName(model.getUiDataModel().getTxtVslName().trim());
                model.getUiDataModel().setTxtNewVoy(model.getUiDataModel().getTxtVoyageCode().trim());
                model.getUiDataModel().setTxtNewBnd(model.getUiDataModel().getTxtBound().trim());
                model.getUiDataModel().setTxtNewOpspod("");
                model.getUiDataModel().setTxtNewOpspodTer("");

                model.getStateModel().setBtnNewSvcDis(true);
                model.getStateModel().setNewSerRO(true);
                model.getStateModel().setBtnNewVslDis(true);
                model.getStateModel().setBtnNewVoyBndDis(true);
            } else {
                model.getUiDataModel().setTxtNewSvc("");
                model.getUiDataModel().setTxtNewVslCode("");
                model.getUiDataModel().setTxtNewVslName("");
                model.getUiDataModel().setTxtNewVoy("");
                model.getUiDataModel().setTxtNewBnd("");
                model.getUiDataModel().setTxtNewOpspod("");
                model.getUiDataModel().setTxtNewOpspodTer("");

                model.getStateModel().setBtnNewSvcDis(false);
                model.getStateModel().setNewSerRO(false);
                model.getStateModel().setBtnNewVslDis(false);
                model.getStateModel().setBtnNewVoyBndDis(false);
            }

            RequestContext.getCurrentInstance().update(FRAGC);
            RequestContext.getCurrentInstance().update(FRAGB);
            RequestContext.getCurrentInstance().update(FRAGB1);
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void copyRecordFromGrid() {
        //            log.info("copyRecordFromGrid called ===");
        if (model.getUiDataModel().getSelectedGridValue() != null) {
            selops = (FDRforcedDischargeVO) model.getUiDataModel().getSelectedGridValue().get(0);
        }
        //            log.info("selops==" + selops.getBookNoL());
        RequestContext.getCurrentInstance().update(TABLE1);
    }

    public void pasteRecordintoGrid() {
        //            log.info("pasteRecordintoGrid called =====");
        String str = selops.toString();
        FDRmatchTableVo vo;
        List ob = new ArrayList();
        String[] bookNo = str.split("#~#");

        //            log.info("str values-------->>" + str);
        for (String bookNo1 : bookNo) {
            if (ob.contains(bookNo1)) {
            } else {
                ob.add(bookNo1);
            }
        }
        ArrayList ar = new ArrayList();
        for (Object ob1 : ob) {
            vo = new FDRmatchTableVo();
            vo.setBkNo(ob1.toString());
            ar.add(vo);
        }
        //            log.info("ar size------>>>" + ar.size());
        model.getUiDataModel().setMatchingGridList(ar);
        RequestContext.getCurrentInstance().update(TABLE2);
    }

    private void refreshResultTable(String compId) {
        DataTable dataTable = (DataTable) FacesContext.getCurrentInstance().getViewRoot().findComponent(compId);
        if (dataTable != null) {
            dataTable.reset();
            dataTable.resetValue();
            RequestContext.getCurrentInstance().reset(compId);
        }
        if (compId.equalsIgnoreCase(TABLE1)) {
            model.getUiDataModel().setFilterlist(null);
            futil.clearFilter(FDR, TABLE1, false);
        }
    }

    public SearchConditionBuilder getDynamicCondition(String favSearchId) {
        try {
            SearchCondition cachecondition;
            switch (favSearchId) {
                case "6020013":
                    dynamicCondition = new SearchConditionBuilder();
                    condition = new ArrayList();
                    condition.add("M");
                    condition.add("F");
                    cachecondition = new SearchCondition("a.mode", SearchConditionBuilder.CONDITION.IN, SearchConditionBuilder.getInOrNotInString(condition));
                    dynamicCondition.getSearchConditionList().add(cachecondition);
                    caseclear();
                    break;

                case "6020014":
                    dynamicCondition = new SearchConditionBuilder();
                    cachecondition = new SearchCondition("ass.agencycode", SearchConditionBuilder.CONDITION.EQUALS, mData.getAgencyCode());
                    dynamicCondition.getSearchConditionList().add(cachecondition);
                    cachecondition = new SearchCondition("a.arrdate", SearchConditionBuilder.CONDITION.BETWEEN, "getDate()-" + popconflt + " and getDate()+" + popconfgt);
                    dynamicCondition.getSearchConditionList().add(cachecondition);
                    break;

                case "6020020":
                    dynamicCondition = new SearchConditionBuilder();
                    cachecondition = new SearchCondition("a.arrdate", SearchConditionBuilder.CONDITION.BETWEEN, "getDate()-" + popconflt + " and getDate()+" + popconfgt);
                    dynamicCondition.getSearchConditionList().add(cachecondition);
                    break;

            }

            //            log.info("dynamicCondition>>>" + dynamicCondition.getConditionAsString());
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
        return dynamicCondition;
    }

    private void caseclear() {

        clearbottomfields();
        model.getUiDataModel().setfDGridList(new ArrayList());
        model.getUiDataModel().setMatchingGridList(new ArrayList());
        refreshResultTable(TABLE1);
        refreshResultTable(TABLE2);
        RequestContext.getCurrentInstance().update(TABLE1);
        RequestContext.getCurrentInstance().update(TABLE2);
    }

    public void mnuRemoveTermsActionPerformed() {
        try {
            log.info("Selected rows to remove " + model.getUiDataModel().getSelectedGridValue1());

            List al = new ArrayList();

            if (!model.getUiDataModel().getSelectedGridValue1().isEmpty()) {

                int nullchk = optionchk2();

                if (nullchk == 0) {
                    dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020047", "", null);
                    FDRforcedDischargeVO vos;
                    for (int i = 0; i < model.getUiDataModel().getfDGridList().size(); i++) {
                        vos = (FDRforcedDischargeVO) model.getUiDataModel().getfDGridList().get(i);
                        vos.setMatchSysL("");
                        vos.setSelectL(false);
                    }
                } else {
                    for (int i = 0; i < model.getUiDataModel().getSelectedGridValue1().size(); i++) {
                        FDRmatchTableVo vo = (FDRmatchTableVo) model.getUiDataModel().getSelectedGridValue1().get(i);

                        if ("Book No / WO No".equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {
                            al.add(vo.getBkNo());
                        } else {
                            al.add(vo.getContainerNo());
                        }
                        model.getUiDataModel().getMatchingGridList().remove(vo);
                    }

                    log.info(">> >> >> >> " + al);
                    optionchk1(al);

                    if (model.getUiDataModel().getMatchingGridList().isEmpty()) {
                        setDefaultEmptyRow();
                    }

                    int nullchks = 0;
                    FDRmatchTableVo vos;
                    for (int i = 0; i < model.getUiDataModel().getMatchingGridList().size(); i++) {
                        vos = (FDRmatchTableVo) model.getUiDataModel().getMatchingGridList().get(i);
                        if ("Book No / WO No".equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {
                            if (vos.getBkNo() != null && vos.getBkNo().length() > 0) {
                                nullchks++;
                            }
                        } else {
                            if (vos.getContainerNo() != null && vos.getContainerNo().length() > 0) {
                                nullchks++;
                            }
                        }
                    }

                    int count = 0;
                    FDRforcedDischargeVO vo1;
                    for (int i = 0; i < model.getUiDataModel().getfDGridList().size(); i++) {
                        vo1 = (FDRforcedDischargeVO) model.getUiDataModel().getfDGridList().get(i);
                        if (!vo1.getMatchSysL().equalsIgnoreCase("")) {
                            count++;
                        }
                    }

                    if (nullchks == 0 || count == 0) {
                        FDRforcedDischargeVO vo;
                        for (int i = 0; i < model.getUiDataModel().getfDGridList().size(); i++) {
                            vo = (FDRforcedDischargeVO) model.getUiDataModel().getfDGridList().get(i);
                            vo.setMatchSysL("");
                            vo.setSelectL(false);
                        }
                    } else {
                        sortbymatch(model.getUiDataModel().getfDGridList(), model.getUiDataModel().getMatchingGridList());
                    }
                }
                refreshResultTable(TABLE1);
                refreshResultTable(TABLE2);
                RequestContext.getCurrentInstance().update("FDR-FDR_tool");
                RequestContext.getCurrentInstance().update(TABLE1);
                RequestContext.getCurrentInstance().update(TABLE2);
            } else {
                dutil.showDialog(FDR, SMDialogUtil.type.INFO, "06020046", "", null);
            }
            model.getUiDataModel().setSelectedGridValue1(new ArrayList());

        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    private int optionchk2() {
        int nullchk = 0;
        FDRmatchTableVo vos;
        for (int i = 0; i < model.getUiDataModel().getMatchingGridList().size(); i++) {
            vos = (FDRmatchTableVo) model.getUiDataModel().getMatchingGridList().get(i);

            if ("Book No / WO No".equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {
                if (vos.getBkNo() != null && vos.getBkNo().length() > 0) {

                    nullchk++;

                }
            } else {
                if (vos.getContainerNo() != null && vos.getContainerNo().length() > 0) {

                    nullchk++;

                }
            }

        }
        return nullchk;
    }

    private void optionchk1(List al) {
        for (int i = 0; i < model.getUiDataModel().getfDGridList().size(); i++) {
            FDRforcedDischargeVO vo1 = (FDRforcedDischargeVO) model.getUiDataModel().getfDGridList().get(i);
            String cntno;

            if ("Book No / WO No".equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {
                cntno = vo1.getBookNoL().toUpperCase().trim();
            } else {
                cntno = vo1.getContainerNoL().toUpperCase().trim();
            }

            if (al.contains(cntno)) {
                vo1.setMatchSysL("UnMatch");
                vo1.setSelectL(false);
            }
        }
    }

    public void chkmatchaction() {
        try {
             
            if (model.getStateModel().isChkmatch()) {
                if (BKGNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {
                    model.getStateModel().setGridhdr1(true);
                    model.getStateModel().setGridhdr2(false);

                    model.getUiDataModel().setBookcntrno("Book No / WO No");
                    model.getUiDataModel().setLblStats("~ Matched Book No / WO No");
                    model.getUiDataModel().setLblStats1("~ Unmatched Book No / WO No");

                } else if (CNTNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {
                    model.getStateModel().setGridhdr1(false);
                    model.getStateModel().setGridhdr2(true);

                    model.getUiDataModel().setBookcntrno(CNTNUMBER);
                    model.getUiDataModel().setLblStats("~ Matched Container No");
                    model.getUiDataModel().setLblStats1("~ Unmatched Container No");

                }else if (VINNO.equalsIgnoreCase(model.getUiDataModel().getButtonGroup2())) {
                    model.getStateModel().setGridhdr1(false);
                    model.getStateModel().setGridhdr2(true);

                    model.getUiDataModel().setBookcntrno(VINNO);
                    model.getUiDataModel().setLblStats("~ Matched VIN/Chassis No");
                    model.getUiDataModel().setLblStats1("~ Unmatched VIN/Chassis No");
                }
                model.getUiDataModel().setGridStyle("sm-margin-right2 sm-table-wrap sm-border");
                model.getUiDataModel().setTable1size("ui-g-12 ui-lg-9 ui-md-12 sm-padding0 sm-margin-top0 sm-input-size84");
                model.getUiDataModel().setTable2size("ui-g-12 ui-lg-3 ui-md-12 sm-padding0 sm-margin-top0 sm-input-size16");
                model.getStateModel().setButtonGroup2Dis(false);
                if (model.getUiDataModel().getfDGridList().isEmpty()) {
                    model.getStateModel().setButtonGroup2Dis(true);
                }

            } else {
                model.getStateModel().setButtonGroup2Dis(true);
                model.getUiDataModel().setGridStyle("sm-table-wrap sm-border");
                model.getUiDataModel().setTable1size("ui-g-12 ui-lg-9 ui-md-12 sm-padding0 sm-margin-top0 sm-input-size100");
                model.getUiDataModel().setTable2size("ui-g-12 ui-lg-3 ui-md-12 sm-padding0 sm-margin-top0 sm-input-size0");

            }
            RequestContext.getCurrentInstance().update("FDR-FDR_FRAGBOTHTABLE");
            RequestContext.getCurrentInstance().update(FRAG1);

        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    public void selectselectedrows() {
        try {
            List gridlist;
            if (model.getUiDataModel().getSelectedGridValue() != null && !model.getUiDataModel().getSelectedGridValue().isEmpty()) {
                gridlist = model.getUiDataModel().getSelectedGridValue();
                for (Object gridlist1 : gridlist) {
                    FDRforcedDischargeVO vo11 = (FDRforcedDischargeVO) gridlist1;
                    if (!vo11.getSelectL()) {
                        vo11.setSelectL(true);
                        model.getUiDataModel().setBtnSelectValue("");
                    } else {
                        vo11.setSelectL(false);
                        model.getUiDataModel().setBtnSelectValue(SELECTALL);
                    }
                }
                model.getUiDataModel().setSelectedGridValue(new ArrayList());
                for (int j = 0; j < model.getUiDataModel().getfDGridList().size(); j++) {
                    FDRforcedDischargeVO vo1 = (FDRforcedDischargeVO) model.getUiDataModel().getfDGridList().get(j);
                    if (vo1.getSelectL()) {
                        model.getUiDataModel().getSelectedGridValue().add(vo1);
                    }
                }
                checkuncheck();
                RequestContext.getCurrentInstance().update(TABLE1);
            }
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
    }

    private String nullChecker(Object strValue) {
        return (strValue != null) ? strValue.toString().trim() : "";
    }
    
    public void radiouncontchange() { 
        try {
            if (model.getStateModel().isChkmatchun()) {
                caseclear();
                model.getStateModel().setGridhdr3(false);
                model.getStateModel().setContflag(false);
                // model.getStateModel().setGridhdr4(true);
            } else {
                caseclear();
                model.getStateModel().setGridhdr3(true);
                model.getStateModel().setContflag(true);
                // model.getStateModel().setGridhdr4(false);
            }
            RequestContext.getCurrentInstance().update(TABLE1);
            RequestContext.getCurrentInstance().update(FRAG1);
        } catch (Exception e) {
            log.info(SOUT);
            log.fatal(e);
        }
        
    }
}
